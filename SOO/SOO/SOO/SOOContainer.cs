﻿using Autofac;
using AutoMapper;
using DTMS.Xamarin.Core.IoC;
using SOO.Commons.Shared.Models;
using SOO.Components;
using SOO.Components.Collection;
using SOO.Components.Home;
using SOO.Components.SplashScreen;
using SOO.Components.Story;
using Xamarin.Forms;

namespace SOO
{
    public class SOOContainer : AppContainer
    {
        protected override void RegisterServices(ContainerBuilder containerBuilder)
        {
            base.RegisterServices(containerBuilder);

            containerBuilder.RegisterType<HomeView>().Named<Page>(ViewNames.HomeView).As<HomeView>().SingleInstance();
            containerBuilder.RegisterType<StoryView>().Named<Page>(ViewNames.StoryView).As<StoryView>().InstancePerDependency();
            containerBuilder.RegisterType<Components.Collection.CollectionView>().Named<Page>(ViewNames.CollectionView).As<Components.Collection.CollectionView>().InstancePerDependency();
            containerBuilder.RegisterType<SplashScreenView>().Named<Page>(ViewNames.SplashScreenView).As<SplashScreenView>().InstancePerLifetimeScope();

            containerBuilder.RegisterType<SplashScreenViewModel>().SingleInstance();
            containerBuilder.RegisterType<HomeViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<StoryViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<CollectionViewModel>().InstancePerDependency();
            
            AddAutoMapper(containerBuilder);
        }

        private void AddAutoMapper(ContainerBuilder containerBuilder)
        {
            containerBuilder.Register(context => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<StoryModel, Story>().ReverseMap();
                cfg.CreateMap<StoryDetailModel, StoryDetail>().ReverseMap();
                cfg.CreateMap<CollectionModel, Collection>().ReverseMap();
            }));
            containerBuilder.Register(ctx => ctx.Resolve<MapperConfiguration>().CreateMapper()).As<IMapper>().InstancePerLifetimeScope();
        }
    }
}

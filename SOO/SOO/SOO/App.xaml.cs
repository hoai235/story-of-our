﻿using System;
using System.Threading.Tasks;
using DTMS.Xamarin.Core.Interfaces;
using NLog;
using SOO.Commons;
using SOO.Components;
using Xamarin.Forms;
using XF.Material.Forms;

namespace SOO
{
    public partial class App : Application
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public App(INavigationService navigationService)
        {
            Device.SetFlags(new[] { "Shapes_Experimental", "SwipeView_Experimental" });
            InitializeComponent();

            Material.Init(this, "Material.Style");

            DTMS.Xamarin.Core.DTMS.Init(Constants.ApplicationName, true);
            navigationService.SetRootView(ViewNames.SplashScreenView);
        }

        protected override void OnStart()
        {
            AppDomain.CurrentDomain.UnhandledException += AppDomainUnhandledException;
        }

        private void AppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            _logger.Error(exception);
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

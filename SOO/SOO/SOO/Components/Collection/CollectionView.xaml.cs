﻿using Xamarin.Forms.Xaml;

namespace SOO.Components.Collection
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CollectionView : BaseCollectionView
    {
        public CollectionView()
        {
            InitializeComponent();
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using DTMS.Xamarin.Core.BaseXamarin;
using DTMS.Xamarin.Core.Behaviors;
using DTMS.Xamarin.Core.Interfaces;
using DTMS.Xamarin.Core.Models;
using Rg.Plugins.Popup.Services;
using SOO.Commons;
using SOO.Commons.Shared.Models;
using SOO.Commons.Shared.Services;
using SOO.Components.Collection.Components;
using Xamarin.Forms;

namespace SOO.Components.Collection
{
    public class CollectionViewModel : BaseViewModel
    {

        #region Avarables

        private string _storyId = string.Empty;
        private readonly ISqliteService _sqliteService;
        private readonly IMapper _mapper;
        private IMediaService _mediaService;

        #endregion Avarables

        /* ============================================================= */

        #region Properities

        private ObservableCollection<CollectionModel> _collections;
        public ObservableCollection<CollectionModel> Collections
        {
            get => _collections;
            set => SetProperty(ref _collections, value);
        }

        private CollectionModel _selectCollection;
        public CollectionModel SelectCollection
        {
            get => _selectCollection;
            set => SetProperty(ref _selectCollection, value, OpenImageFullScreen);
        }

        #endregion Properities

        /* ============================================================= */

        #region Commands

        public IAsyncCommand BackCommand { get; set; }
        public IActionCommand ItemTappedCommand { get; set; }
        public IAsyncCommand AddCollectionCommand { get; set; }

        #endregion Commands

        /* ============================================================= */

        public CollectionViewModel(ISqliteService sqliteService, IMapper mapper)
        {
            _sqliteService = sqliteService;
            _mapper = mapper;

            InitMediaService();
            InitCommands();
        }

        ~CollectionViewModel()
        {
            _mediaService.OnMediaPicked -= OnMediaPicked;
        }

        /* ============================================================= */

        #region Functions
        
        private void InitMediaService()
        {
            _mediaService = DependencyService.Get<IMediaService>();
            _mediaService.OnMediaPicked += OnMediaPicked;
        }

        private void OnMediaPicked(object sender, MediaFile image)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var model = new Commons.Shared.Models.Collection
                {
                    StoryId = _storyId,
                    ImagePath = image.Path,
                    ThumbnailPath = image.PreviewPath,
                    Type = image.Type.ToString()
                };

                await _sqliteService.InsertAsync(model);

                MessagingCenter.Send(string.Empty, Constants.MessageCenters.UPDATE_TOTAL_GALLEY);

                Collections.Add(new CollectionModel
                {
                    ImagePath = image.Path,
                    ThumbnailPath = image.PreviewPath,
                    StoryId = _storyId,
                    Type = image.Type.ToString().ToLower(CultureInfo.InvariantCulture)
                });
            });
        }

        private void InitCommands()
        {
            BackCommand = new AsyncCommand(async () => await NavigationService.PopModalAsync());
            AddCollectionCommand = new AsyncCommand(AddCollection);
        }

        public override async void OnViewPushed(object navigationParameter = null)
        {
            _storyId = (string)navigationParameter;
            var result = await _sqliteService.GetAllAsync<Commons.Shared.Models.Collection>(x => x.StoryId == _storyId);
            Collections = new ObservableCollection<CollectionModel>(_mapper.Map<IEnumerable<CollectionModel>>(result));
        }

        private async Task AddCollection()
        {
            await _mediaService.PickPhotosAsync();
        }

        private async void OpenImageFullScreen()
        {
            if (SelectCollection == null) 
                return;

            var imagePage = new ImagePage(SelectCollection.ImagePath);
            await PopupNavigation.Instance.PushAsync(imagePage);
            SelectCollection = null;
        }

        #endregion Functions

    }
}

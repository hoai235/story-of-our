﻿using System;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SOO.Commons;
using Xamarin.Forms;

namespace SOO.Components.Collection.Components
{
    public partial class ImagePage : PopupPage
    {
        public ImagePage(string path)
        {
            InitializeComponent();
            ImgCollection.Source = ImageSource.FromFile(path);
        }

        protected override void OnAppearing()
        {
            MessagingCenter.Send("true", Constants.MessageCenters.SCREEN_ORIENTATION);
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Send("false", Constants.MessageCenters.SCREEN_ORIENTATION);
            base.OnDisappearing();
        }

        private async void OnClose(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAllAsync();
        }
    }
}
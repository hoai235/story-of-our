﻿using DTMS.Xamarin.Core.Helpers;
using DTMS.Xamarin.Core.Interfaces;
using System.IO;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Components.SplashScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashScreenView : BaseSplashScreenView
    {
        public SplashScreenView()
        {
            InitializeComponent();

            RequestPermisstion();
        }

        private async void RequestPermisstion()
        {
            await Task.Delay(3000)
                  .ContinueWith((t) =>
                  {
                      Device.BeginInvokeOnMainThread(async () =>
                      {
                          var granted = await this.ViewModel.RequestPermissionStorage();

                          if (granted)
                          {
                              DependencyService.Get<IDTMSAndroidFeatureService>().GetFolderLocation(ApplicationHelper.AppName);

                              this.ViewModel.InitializeApplication();
                          }
                          else
                          {
                              this.ViewModel.CloseApp();
                          }
                      });
                  });
        }
    }
}
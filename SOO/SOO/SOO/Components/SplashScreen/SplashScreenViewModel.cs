﻿using DTMS.Xamarin.Core.BaseXamarin;
using DTMS.Xamarin.Core.Helpers;
using DTMS.Xamarin.Core.Interfaces;
using NLog;
using Plugin.Permissions.Abstractions;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DTMS.Xamarin.Core.Behaviors;
using SOO.Commons.Shared.Models;

namespace SOO.Components.SplashScreen
{
    public class SplashScreenViewModel : BaseViewModel
    {
        #region Avariables

        private readonly IPermissionService _permissionService;

        private readonly ISqliteService _sqliteService;

        #endregion Avariables

        /* ============================================================== */

        #region Properities

        #endregion Properities

        /* ============================================================== */

        #region Commands

        public IActionCommand<string> ShowMainPageCommand { get; set; }

        #endregion Commands

        /* ============================================================== */

        #region Constructor

        public SplashScreenViewModel(IPermissionService permissionService, ISqliteService sqliteService)
        {
            this._permissionService = permissionService;
            this._sqliteService = sqliteService;

            InitializeCommand();
        }

        #endregion Constructor

        /* ============================================================== */

        #region Functions

        private void InitializeCommand()
        {
            ShowMainPageCommand = new ActionCommand<string>((pageName) => NavigationService.SetRootView(pageName));
        }

        public void InitializeApplication()
        {
            var folderLog = AndroidService.GetFolderLocation(ApplicationHelper.GetFolderLog());

            LogManager.Configuration = LoggingHelper.GetConfiguration(folderLog);

            Logger.Info("Start application");

            if (ApplicationHelper.UserSqlite)
            {
                var folderDatabase = AndroidService.GetFolderLocation(ApplicationHelper.GetFolderDatabase());
                var databaseFilePath = Path.Combine(folderDatabase, ApplicationHelper.GetSqliteName());

                _sqliteService.InitializeConnection(databaseFilePath);
                _sqliteService.CreateTables(typeof(Commons.Shared.Models.Story));
                //MigrationData();

                Logger.Info("Initialize connect sqlite success");
            }

            ShowMainPageCommand.Execute(ViewNames.HomeView);
        }

        public async Task<bool> RequestPermissionStorage()
        {
            var isGranted = await _permissionService.GetPermissionStatusAsync(Permission.Storage);

            return isGranted == PermissionStatus.Granted;
        }

        private async void MigrationData()
        {
            var stories = await _sqliteService.GetAllAsync<Commons.Shared.Models.Story>(x => x.TotalCost == 0);
            foreach (var story in stories)
            {
                var details = await _sqliteService.GetAllAsync<StoryDetail>(x => x.StoryId == story.Id);

                if (details.Any())
                {
                    story.TotalCost = details.Sum(x => x.Cost);
                    await _sqliteService.UpdateAsync(story);
                    var order = 1;
                    foreach (var detail in details)
                    {
                        detail.Order = order;
                        order += 1;
                    }

                    await _sqliteService.UpdateRangeAsync(details);
                }
            }
        }

        #endregion Functions
    }
}

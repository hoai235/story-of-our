﻿namespace SOO.Components
{
    public static class ViewNames
    {
        public const string HomeView = nameof(Home.HomeView);
        public const string StoryView = nameof(Story.StoryView);
        public const string SplashScreenView = nameof(SplashScreen.SplashScreenView);
        public const string CollectionView = nameof(Collection.CollectionView);
    }
}

﻿using DTMS.Xamarin.Core.BaseXamarin;
using SOO.Components.Collection;
using SOO.Components.Home;
using SOO.Components.SplashScreen;
using SOO.Components.Story;

namespace SOO.Components
{
    public class BaseHomeView : BaseContentPage<HomeViewModel> { }
    public class BaseStoryView : BaseContentPage<StoryViewModel> { }
    public class BaseSplashScreenView : BaseContentPage<SplashScreenViewModel> { }
    public class BaseCollectionView : BaseContentPage<CollectionViewModel> { }
}

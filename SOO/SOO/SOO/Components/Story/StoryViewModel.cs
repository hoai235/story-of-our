﻿using DTMS.Xamarin.Core.BaseXamarin;
using DTMS.Xamarin.Core.Interfaces;
using SOO.Commons.Shared.Models;
using SOO.Components.Story.Components;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using DTMS.Xamarin.Core.Behaviors;
using SOO.Commons;
using Xamarin.Forms;
using XF.Material.Forms.Models;

namespace SOO.Components.Story
{
    public class StoryViewModel : BaseViewModel
    {

        #region Avarables

        private StoryModel _story;
        private readonly ISqliteService _sqliteService;
        private readonly IMapper _mapper;

        #endregion Avarables

        /* ============================================================= */

        #region Properities

        private ObservableCollection<StoryDetailModel> _storyDetails;
        public ObservableCollection<StoryDetailModel> StoryDetails
        {
            get => _storyDetails;
            set => SetProperty(ref _storyDetails, value, UpdateOrderDetails);
        }

        private int _totalCost;
        public int TotalCost
        {
            get => _totalCost;
            set => SetProperty(ref _totalCost, value);
        }

        #endregion Properities

        /* ============================================================= */

        #region Commands

        public IAsyncCommand BackCommand { get; set; }
        public IAsyncCommand AddStoryDetailCommand { get; set; }
        public IAsyncCommand CollectionCommand { get; set; }
        public IAsyncCommand<StoryDetailModel> EditStoryDetailCommand { get; set; }
        public IAsyncCommand<StoryDetailModel> DeleteStoryDetailCommand { get; set; }
        public ICommand ChoiceMenuCommand { get; set; }

        #endregion Commands

        /* ============================================================= */

        public StoryViewModel(ISqliteService sqliteService, IMapper mapper)
        {
            _sqliteService = sqliteService;
            _mapper = mapper;

            InitCommands();
            InitActions();
        }

        /* ============================================================= */

        #region Functions

        private void InitActions()
        {
            Actions = new List<string>
            {
                "Chỉnh sửa",
                "Ghi chú",
                "Xóa"
            };
        }

        private void InitCommands()
        {
            BackCommand = new AsyncCommand(async () => await NavigationService.PopAsync());
            CollectionCommand = new AsyncCommand(async() => await NavigationService.PushModalAsync(ViewNames.CollectionView, _story.Id));
            AddStoryDetailCommand = new AsyncCommand(CreateStoryDetail);
            EditStoryDetailCommand = new AsyncCommand<StoryDetailModel>(UpdateStoryDetail);
            DeleteStoryDetailCommand = new AsyncCommand<StoryDetailModel>(DeleteStoryDetail);
            ChoiceMenuCommand = new Command<MaterialMenuResult>(async (result) =>
            {
                var model = (StoryDetailModel) result.Parameter;
                switch (result.Index)
                {
                    case 0:
                        await UpdateStoryDetail(model);
                        break;
                    case 1:
                        await UpdateNote(model.Id);
                        break;
                    case 2:
                        await DeleteStoryDetail(model);
                        break;
                    default:
                        return;
                }
            });
        }

        private async Task CreateStoryDetail()
        {
            var dialog = new CreateStoryDetailDialog();

            var isSave = await DialogService.ShowView(dialog, "Tạo mới chi tiết", null, "Lưu", "Đóng");

            if (isSave == null || isSave == false)
            {
                return;
            }

            var model = dialog.InitData();

            if (string.IsNullOrEmpty(model.Title.Trim()) || !DateTime.TryParse(model.CreateDate.ToString(CultureInfo.CurrentCulture), out _))
            {
                await SnackbarService.ShowMessageButton("Vui lòng nhập đủ thông tin", "Thử lại", async () => await CreateStoryDetail());
                return;
            }

            model.Order = StoryDetails.Count > 0 ? StoryDetails.Count + 1 : 1;
            model.StoryId = _story.Id;

            await _sqliteService.InsertAsync(_mapper.Map<StoryDetail>(model));
            await UpdateTotalCostStory();

            StoryDetails.Add(model);
            UpdateTotalCost();

            MessagingCenter.Send(new MessagingModel<int>
            {
                Id = _story.Id, 
                Data = TotalCost
            }, Constants.MessageCenters.UPDATE_TOTAL_COST);

            await SnackbarService.ShowSuccess("Tạo mới thành công");
        }

        private async Task UpdateStoryDetail(StoryDetailModel model)
        {
            var dialog = new UpdateStoryDetailDialog(model);

            var isSave = await DialogService.ShowView(dialog, "Chỉnh sửa chi tiết", null, "Lưu", "Đóng");

            if (isSave == null || isSave == false)
            {
                return;
            }

            var data = dialog.UpdateData();

            if (string.IsNullOrEmpty(data.Title.Trim()) || !DateTime.TryParse(data.CreateDate.ToString(CultureInfo.CurrentCulture), out _))
            {
                await SnackbarService.ShowMessageButton("Vui lòng nhập đủ thông tin", "Thử lại", async () => await UpdateStoryDetail(model));
                return;
            }

            await _sqliteService.UpdateAsync(_mapper.Map<StoryDetail>(data));
            await UpdateTotalCostStory();

            var newModel = StoryDetails.FirstOrDefault(x => x.Id == model.Id);
            newModel.Title = data.Title;
            newModel.CreateDate = data.CreateDate;
            newModel.Cost = model.Cost;
            UpdateTotalCost();

            MessagingCenter.Send(new MessagingModel<int>
            {
                Id = _story.Id,
                Data = TotalCost
            }, Constants.MessageCenters.UPDATE_TOTAL_COST);

            await SnackbarService.ShowSuccess("Chỉnh sửa thành công");
        }

        private async Task DeleteStoryDetail(StoryDetailModel model)
        {
            var isConfirm = await DialogService.Confirm(
                "Thông báo"
                , "Mỡ Mỡ có muốn xóa chi tiết này ?"
                , "Chấp nhận"
                , "Đóng");

            if (isConfirm == null || isConfirm == false)
                return;

            await _sqliteService.DeleteAsync(new StoryDetail { Id = model.Id });

            StoryDetails.Remove(model);
            UpdateTotalCost();

            await SnackbarService.ShowSuccess("Xóa thành công");
        }

        private async Task UpdateNote(string id)
        {
            var data = StoryDetails.FirstOrDefault(x => x.Id == id);
            var dialog = new UpdateNoteDialog(data.Note);

            var isSave = await DialogService.ShowView(dialog, "Ghi chú", null, "Lưu", "Đóng");

            if (isSave == null || isSave == false)
            {
                return;
            }

            var note = dialog.GetNote();
            data.Note = note;
            await _sqliteService.UpdateAsync(_mapper.Map<StoryDetail>(data));
        }

        public override async void OnViewPushed(object navigationParameter = null)
        {
            var story = (StoryModel)navigationParameter;

            if (story == null)
            {
                BackCommand.Execute(null);
            }

            MessagingCenter.Send(new MessagingModel<string>(), Constants.MessageCenters.RESET_SELECT_STORY);

            _story = story;

            var storyDetail = await _sqliteService.GetAllAsync<StoryDetail>(x => x.StoryId == _story.Id);
            var sortData = storyDetail.OrderBy(x => x.Order);
            var s = _mapper.Map<IEnumerable<StoryDetailModel>>(sortData);
            StoryDetails = new ObservableCollection<StoryDetailModel>(s);
            UpdateTotalCost();
        }

        private async void UpdateOrderDetails()
        {
            if (StoryDetails != null && StoryDetails.Any())
            {
                await _sqliteService.UpdateRangeAsync(_mapper.Map<IEnumerable<StoryDetail>>(StoryDetails));
            }
        }

        private void UpdateTotalCost()
        {
            TotalCost = StoryDetails.Sum(x => x.Cost);
        }

        private async Task UpdateTotalCostStory()
        {
            _story.TotalCost = TotalCost;
            await _sqliteService.UpdateAsync(_mapper.Map<Commons.Shared.Models.Story>(_story));
        }

        #endregion Functions

    }
}

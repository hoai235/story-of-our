﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Components.Story.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateNoteDialog : ContentView
    {
        public UpdateNoteDialog(string note)
        {
            InitializeComponent();

            txfNote.Text = note;
        }

        public string GetNote()
        {
            return txfNote.Text;
        }
    }
}
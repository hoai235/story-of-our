﻿using SOO.Commons.Shared.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Components.Story.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateStoryDetailDialog : ContentView
    {
        private readonly StoryDetailModel _storyDetail;

        public UpdateStoryDetailDialog(StoryDetailModel model)
        {
            InitializeComponent();
            _storyDetail = model;
            txfTitle.Text = _storyDetail.Title;
            txfCost.Text = _storyDetail.Cost.ToString();
            dtfCreateDate.Date = _storyDetail.CreateDate;
        }

        public StoryDetailModel UpdateData()
        {
            return new StoryDetailModel
            {
                Id = _storyDetail.Id,
                StoryId = _storyDetail.StoryId,
                CreateDate = dtfCreateDate.Date.GetValueOrDefault(),
                Cost = int.Parse(txfCost.Text),
                Order = _storyDetail.Order,
                Title = txfTitle.Text
            };
        }
    }
}
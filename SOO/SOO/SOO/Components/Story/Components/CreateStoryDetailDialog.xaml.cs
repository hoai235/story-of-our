﻿using System;
using SOO.Commons.Shared.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Components.Story.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateStoryDetailDialog : ContentView
    {
        public CreateStoryDetailDialog()
        {
            InitializeComponent();

            dtfCreateDate.Date = DateTime.Now;
            txfCost.Text = 0.ToString();
        }

        public StoryDetailModel InitData()
        {
            return new StoryDetailModel
            {
                Id = Guid.NewGuid().ToString().Replace("-", ""),
                Title = txfTitle.Text,
                CreateDate = dtfCreateDate.Date.GetValueOrDefault(),
                Cost = int.Parse(txfCost.Text)
            };
        }
    }
}
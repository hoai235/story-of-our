﻿using Xamarin.Forms.Xaml;

namespace SOO.Components.Story
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StoryView : BaseStoryView
    {
        public StoryView()
        {
            InitializeComponent();
        }
    }
}
﻿using DTMS.Xamarin.Core.BaseXamarin;
using DTMS.Xamarin.Core.Extensions;
using DTMS.Xamarin.Core.Interfaces;
using SOO.Components.Home.Components;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DTMS.Xamarin.Core.Behaviors;
using DTMS.Xamarin.Core.Utilities;
using SOO.Commons;
using SOO.Commons.Shared.Models;
using Xamarin.Forms;

namespace SOO.Components.Home
{
    public class HomeViewModel : BaseViewModel
    {

        #region Avariables

        private readonly ISqliteService _sqliteService;
        private readonly IMapper _mapper;

        #endregion Avariables

        /* ============================================================= */

        #region Properities

        private ObservableCollection<StoryModel> _stories;

        public ObservableCollection<StoryModel> Stories
        {
            get => _stories;
            set => SetProperty(ref _stories, value, CalculateTotalReport);
        }

        private StoryModel _selectStory;

        public StoryModel SelectStory
        {
            get => _selectStory;
            set => SetProperty(ref _selectStory, value, ViewDetailStory);
        }

        private int _totalStory;
        public int TotalStory
        {
            get => _totalStory;
            set => SetProperty(ref _totalStory, value);
        }

        private int _totalCost;

        public int TotalCost
        {
            get => _totalCost;
            set => SetProperty(ref _totalCost, value);
        }

        private int _totalGalley;

        public int TotalGalley
        {
            get => _totalGalley;
            set => SetProperty(ref _totalGalley, value);
        }

        private bool _isRefreshingData;

        public bool IsRefreshingData
        {
            get => _isRefreshingData;
            set => SetProperty(ref _isRefreshingData, value);
        }

        #endregion Properities

        /* ============================================================= */

        #region Commands

        public IAsyncCommand CreateStoryCommand { get; set; }
        public IActionCommand RefreshCommand { get; set; }
        public IAsyncCommand<StoryModel> EditStoryCommand { get; set; }
        public IAsyncCommand<StoryModel> DeleteStoryCommand { get; set; }
        public IActionCommand<int> LoadMoreCommand { get; set; }

        #endregion Commands

        /* ============================================================= */

        public HomeViewModel(ISqliteService sqliteService, IMapper mapper)
        {
            _sqliteService = sqliteService;
            _mapper = mapper;

            InitCommands();

            LoadData();

            RegisterMessaging();
        }

        /* ============================================================= */

        #region Functions

        public void InitCommands()
        {
            CreateStoryCommand = new AsyncCommand(CreateStory);
            RefreshCommand = new ActionCommand(() =>
            {
                Stories = null;
                IsRefreshingData = true;
                
                LoadData();

                IsRefreshingData = false;
            });
            EditStoryCommand = new AsyncCommand<StoryModel>(UpdateStory);
            DeleteStoryCommand = new AsyncCommand<StoryModel>(DeleteStory);
            LoadMoreCommand = new ActionCommand<int>((page) => LoadData(page));
        }

        private async void ViewDetailStory()
        {
            if (SelectStory != null)
            {
                MessagingCenter.Send(string.Empty, Constants.MessageCenters.CLOSE_SWIPE_ITEM);
                await NavigationService.PushAsync(ViewNames.StoryView, SelectStory);
            }
        }

        private async Task CreateStory()
        {
            MessagingCenter.Send(string.Empty, Constants.MessageCenters.CLOSE_SWIPE_ITEM);
            var dialog = new CreateStoryDialog();
            var isSave = await DialogService.ShowView(dialog, "Tạo chuyến đi", confirmingText: "Lưu", dismissiveText: "Đóng");

            if (isSave == null || isSave == false)
            {
                return;
            }

            var data = dialog.InitData();

            if (string.IsNullOrEmpty(data.Title.Trim()) || !DateTime.TryParse(data.CreateDate.ToString(CultureInfo.CurrentCulture), out _))
            {
                await SnackbarService.ShowMessageButton("Vui lòng nhập đủ thông tin", "Thử lại", async () => await CreateStory());
                return;
            }

            await _sqliteService.InsertAsync(_mapper.Map<Commons.Shared.Models.Story>(data));

            Stories.AddFirst(data);
            CalculateTotalStory();
            await SnackbarService.ShowSuccess("Tạo mới thành công");
        }

        private async Task UpdateStory(StoryModel model)
        {
            var dialog = new UpdateStoryDialog(model);

            var isSave = await DialogService.ShowView(dialog, "Chỉnh sửa chuyến đi", confirmingText: "Lưu", dismissiveText: "Đóng");
 
            if (isSave == null || isSave == false)
            {
                return;
            }

            var data = dialog.UpdateData();

            if (string.IsNullOrEmpty(data.Title.Trim()) || !DateTime.TryParse(data.CreateDate.ToString(CultureInfo.CurrentCulture), out _))
            {
                await SnackbarService.ShowMessageButton("Vui lòng nhập đủ thông tin", "Thử lại", async () => await UpdateStory(model));
                return;
            }

            await _sqliteService.UpdateAsync(_mapper.Map<Commons.Shared.Models.Story>(data));

            var newModel = Stories.FirstOrDefault(x => x.Id == model.Id);
            newModel.Title = data.Title;
            newModel.CreateDate = data.CreateDate;
            newModel.TotalCost = model.TotalCost;

            await SnackbarService.ShowSuccess("Chỉnh sửa thành công");
        }

        public async Task DeleteStory(StoryModel model)
        {
            var isConfirm = await DialogService.Confirm(
                "Thông báo"
                , "Mỡ Mỡ có muốn xóa câu chuyện này ?"
                , "Chấp nhận"
                , "Đóng");

            if (isConfirm == null || isConfirm == false)
                return;

            await _sqliteService.DeleteAsync(new Commons.Shared.Models.Story { Id = model.Id });

            Stories.Remove(model);
            CalculateTotalStory();
            CalculateTotalGalley();

            await SnackbarService.ShowSuccess("Xóa thành công");
        }

        public async void LoadData(int page = 1, int pageSize = 10)
        {
            if(Stories == null || !Stories.Any())
            {
                Stories = new ObservableCollection<StoryModel>();
            }

            var story = await _sqliteService.GetAllAsync<Commons.Shared.Models.Story>();
            var pagingData = story.OrderByDescending(x => x.CreateDate).Paging(page, pageSize);

            var result = pagingData.Select(data => new StoryModel
            {
                CreateDate = data.CreateDate, 
                Title = data.Title, 
                TotalCost = data.TotalCost, 
                Id = data.Id
            }).ToList();

            Stories.AddRange(result);
            CalculateTotalReport();
        }

        private void CalculateTotalReport()
        {
            if (Stories == null || Stories.Count <= 0) 
                return;

            CalculateTotalStory();
            CalculateTotalCost();
            CalculateTotalGalley();
        }

        private void CalculateTotalCost()
        {
            TotalCost = Stories.Sum(x => x.TotalCost);
        }

        private void CalculateTotalStory()
        {
            TotalStory = Stories.Count;
        }

        private async void CalculateTotalGalley()
        {
            TotalGalley = await _sqliteService.Count<Commons.Shared.Models.Collection>();
        }

        private void RegisterMessaging()
        {
            MessagingCenter.Subscribe<MessagingModel<int>>(this, Constants.MessageCenters.UPDATE_TOTAL_COST, (data) =>
            {
                var story = Stories.First(x => x.Id == data.Id);
                story.TotalCost = data.Data;
                CalculateTotalCost();
            });
            MessagingCenter.Subscribe<string>(this, Constants.MessageCenters.UPDATE_TOTAL_GALLEY, (data) =>
            {
                CalculateTotalGalley();
            });
            MessagingCenter.Subscribe<MessagingModel<string>>(this, Constants.MessageCenters.RESET_SELECT_STORY, (data) => SelectStory = null);
        }

        #endregion Functions
    }
}


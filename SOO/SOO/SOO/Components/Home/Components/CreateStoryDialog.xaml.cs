﻿using System;
using SOO.Commons.Shared.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Components.Home.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateStoryDialog : ContentView
    {
        public CreateStoryDialog()
        {
            InitializeComponent();

            dtfCreatedDate.Date = DateTime.Now;
        }

        public StoryModel InitData()
        {
            return new StoryModel
            {
                Id = Guid.NewGuid().ToString().Replace("-", ""),
                Title = txfTitle.Text,
                CreateDate = dtfCreatedDate.Date.GetValueOrDefault()
            };
        }
    }
}
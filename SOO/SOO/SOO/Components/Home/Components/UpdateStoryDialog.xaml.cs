﻿using SOO.Commons.Shared.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Components.Home.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateStoryDialog : ContentView
    {
        private readonly StoryModel _story;

        public UpdateStoryDialog(StoryModel story)
        {
            InitializeComponent();
            _story = story;
            txfTitle.Text = _story.Title;
            dtfCreatedDate.Date = _story.CreateDate;
        }

        public StoryModel UpdateData()
        {
            return new StoryModel
            {
                Id = _story.Id,
                TotalCost = _story.TotalCost,
                CreateDate = dtfCreatedDate.Date.GetValueOrDefault(),
                Title = txfTitle.Text,
            };
        }
    }
}
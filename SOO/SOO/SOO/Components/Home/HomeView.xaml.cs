﻿using System;
using System.Collections.Generic;
using System.Linq;
using SOO.Commons;
using SOO.Commons.Shared.Models;
using Xamarin.Forms;

namespace SOO.Components.Home
{
    public partial class HomeView : BaseHomeView
    {
        private List<SwipeView> SwipeViews { get; }

        public HomeView()
        {
            InitializeComponent();

            SwipeViews = new List<SwipeView>();

            MessagingCenter.Subscribe<string>(this,Constants.MessageCenters.CLOSE_SWIPE_ITEM, (sender) =>
            {
                CloseSwipeItem();
            });
        }

        private void ltvStory_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var items = ViewModel.Stories;

            if (items == null || items.Count == 0 || (items.Count <= Constants.PageSize - 1))
                return;

            if((StoryModel)e.Item == items[items.Count - 1])
            {
                var page = (items.Count / Constants.PageSize) + 1;
                ViewModel.LoadMoreCommand.Execute(page);
            }
        }

        private void Cell_OnAppearing(object sender, EventArgs e)
        {
            //var viewCell = (SOOViewCell)sender;

            //await viewCell.View.TranslateTo(-300, 0, easing: Easing.SinInOut);
            //await viewCell.View.TranslateTo(0, 0, easing: Easing.SinInOut);

            //await Task.Delay(100);
        }

        private void OnSwipeEnded(object sender, SwipeEndedEventArgs e)
        {
            SwipeViews.Add((SwipeView)sender);
        }

        private void OnSwipeStarted(object sender, SwipeStartedEventArgs e)
        {
            CloseSwipeItem();
        }

        private void CloseSwipeItem()
        {
            if (SwipeViews.Count != 1)
                return;

            var swipeView = SwipeViews.First();
            swipeView.Close();
            SwipeViews.Remove(swipeView);
        }
    }
}
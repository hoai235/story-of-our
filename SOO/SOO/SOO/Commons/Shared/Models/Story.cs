﻿using DTMS.Xamarin.Core.Models;

namespace SOO.Commons.Shared.Models
{
    public class Story : BaseEntitySqlite
    {
        public string Title { get; set; }
        public int TotalCost { get; set; }
    }
}

﻿using DTMS.Xamarin.Core.Models;

namespace SOO.Commons.Shared.Models
{
    public class StoryDetail : BaseEntitySqlite
    {
        public string StoryId { get; set; }
        public string Title { get; set; }
        public int Cost { get; set; }
        public string Note { get; set; }
        public int Order { get; set; }
    }
}

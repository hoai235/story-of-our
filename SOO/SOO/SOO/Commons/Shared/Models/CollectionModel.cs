﻿using DTMS.Xamarin.Core.Models;

namespace SOO.Commons.Shared.Models
{
    public class CollectionModel : ObservableObject
    {
        private string _storyId;
        public string StoryId
        {
            get => _storyId;
            set => SetProperty(ref _storyId, value);
        }

        private string _imagePath;
        public string ImagePath
        {
            get => _imagePath;
            set => SetProperty(ref _imagePath, value);
        }

        private string _thumbnailPath;
        public string ThumbnailPath
        {
            get => _thumbnailPath;
            set => SetProperty(ref _thumbnailPath, value);
        }

        private string _type;
        public string Type
        {
            get => _type;
            set => SetProperty(ref _type, value);
        }
    }
}

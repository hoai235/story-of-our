﻿using System;
using DTMS.Xamarin.Core.Models;

namespace SOO.Commons.Shared.Models
{
    public class StoryModel : ObservableObject
    {
        public string Id { get; set; }

        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private int _totalCost;
        public int TotalCost
        {
            get => _totalCost;
            set => SetProperty(ref _totalCost, value);
        }

        private DateTime _createDate;
        public DateTime CreateDate
        {
            get => _createDate;
            set => SetProperty(ref _createDate, value);
        }
    }
}

﻿using DTMS.Xamarin.Core.Models;

namespace SOO.Commons.Shared.Models
{
    public class Collection : BaseEntitySqlite
    {
        public string StoryId { get; set; }
        public string ImagePath { get; set; }
        public string Type { get; set; }
        public string ThumbnailPath { get; set; }
    }
}

﻿using System;
using DTMS.Xamarin.Core.Models;

namespace SOO.Commons.Shared.Models
{
    public class StoryDetailModel : ObservableObject
    {
        private string _id;
        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        private string _storyId;
        public string StoryId
        {
            get => _storyId;
            set => SetProperty(ref _storyId, value);
        }

        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private int _cost;
        public int Cost
        {
            get => _cost;
            set => SetProperty(ref _cost, value);
        }

        private string _note;

        public string Note
        {
            get => _note;
            set => SetProperty(ref _note, value);
        }

        private DateTime _createDate;
        public DateTime CreateDate
        {
            get => _createDate;
            set => SetProperty(ref _createDate, value);
        }

        private int _order;
        public int Order
        {
            get => _order;
            set => SetProperty(ref _order, value);
        }
    }
}

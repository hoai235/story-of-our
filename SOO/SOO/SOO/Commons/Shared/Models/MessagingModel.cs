﻿namespace SOO.Commons.Shared.Models
{
    public class MessagingModel<T>
    {
        public string Id { get; set; }
        public T Data { get; set; }
    }
}

﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Commons.Shared.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BackgroundComponent : ContentView
    {
        public BackgroundComponent()
        {
            InitializeComponent();
        }
    }
}
﻿using DTMS.Xamarin.Core.Helpers;
using System;
using System.Globalization;
using Xamarin.Forms;
using XF.Material.Forms;
using XF.Material.Forms.Resources;

namespace SOO.Commons.Shared.Converters
{
    public class ChangeColorNoteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string.IsNullOrEmpty((string) value))
                ? Color.Transparent
                : Material.GetResource<Color>(MaterialConstants.Color.SECONDARY);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

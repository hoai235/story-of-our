﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Commons.Shared.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SOOReportSquare : ContentView
    {
        private const string TitleDefault = "Title";
        private const int DataDefault = 0;

        public SOOReportSquare()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty TitleProperty =
            BindableProperty.Create(nameof(Title), typeof(string), typeof(SOOReportSquare), TitleDefault);

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static readonly BindableProperty DataProperty =
            BindableProperty.Create(nameof(Data), typeof(int), typeof(SOOReportSquare), DataDefault);

        public int Data
        {
            get => (int)GetValue(DataProperty);
            set => SetValue(DataProperty, value);
        }
    }
}
﻿using Xamarin.Forms;

namespace SOO.Commons.Shared.Controls
{
    public class SOOListView : ListView
    {
        public SOOListView() : base(ListViewCachingStrategy.RecycleElement)
        {
        }
    }
}

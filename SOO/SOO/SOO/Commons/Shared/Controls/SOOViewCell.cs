﻿using Xamarin.Forms;

namespace SOO.Commons.Shared.Controls
{
    public class SOOViewCell : ViewCell
    {
        public SOOViewCell()
        {

        }

        public static readonly BindableProperty SelectedItemColorProperty =
            BindableProperty.Create(nameof(SelectedItemColor), typeof(Color), typeof(SOOViewCell), Color.Transparent);

        public Color SelectedItemColor
        {
            get => (Color)GetValue(SelectedItemColorProperty);
            set => SetValue(SelectedItemColorProperty, value);
        }

        public static readonly BindableProperty BackgroundColorProperty =
            BindableProperty.Create(nameof(BackgroundColor), typeof(Color), typeof(SOOViewCell), Color.Transparent);

        public Color BackgroundColor
        {
            get => (Color)GetValue(BackgroundColorProperty);
            set => SetValue(BackgroundColorProperty, value);
        }
    }
}

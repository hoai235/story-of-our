﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;

namespace SOO.Commons.Shared.Controls
{
    public partial class SOOGradientView : ContentView
    {
        public SOOGradientView()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty StartColorProperty =
            BindableProperty.Create(nameof(StartColor), typeof(Color), typeof(SOOGradientView), Color.Blue);

        public Color StartColor
        {
            get => (Color)GetValue(StartColorProperty);
            set => SetValue(StartColorProperty, value);
        }

        public static readonly BindableProperty EndColorProperty =
            BindableProperty.Create(nameof(EndColor), typeof(Color), typeof(SOOGradientView), Color.Red);

        public Color EndColor
        {
            get => (Color)GetValue(EndColorProperty);
            set => SetValue(EndColorProperty, value);
        }

        public static readonly BindableProperty HorizontalProperty =
            BindableProperty.Create(nameof(EndColor), typeof(bool), typeof(SOOGradientView), false);

        public bool Horizontal
        {
            get => (bool)GetValue(HorizontalProperty);
            set => SetValue(HorizontalProperty, value);
        }

        private void OnPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            var info = args.Info;
            var surface = args.Surface;
            var canvas = surface.Canvas;

            canvas.Clear();

            var startPoint = new SKPoint(0, 0);
            var endPoint = Horizontal ? new SKPoint(info.Width, 0) : new SKPoint(0, info.Height);

            using (var paint = new SKPaint())
            {
                var rect = new SKRect(0, 0, info.Width, info.Height);

                // Create linear gradient from upper-left to lower-right
                paint.Shader = SKShader.CreateLinearGradient(startPoint, endPoint, 
                                                             new SKColor[] { StartColor.ToSKColor(), EndColor.ToSKColor() },
                                                             null, SKShaderTileMode.Clamp);

                // Draw the gradient on the rectangle
                canvas.DrawRect(rect, paint);
            }
        }
    }
}
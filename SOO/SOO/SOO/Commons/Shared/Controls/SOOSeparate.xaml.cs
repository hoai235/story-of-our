﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SOO.Commons.Shared.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SOOSeparate : ContentView
    {
        private const string TitleDefault = "Title";

        public SOOSeparate()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty TitleProperty =
            BindableProperty.Create(nameof(Title), typeof(string), typeof(SOOSeparate), TitleDefault);

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static readonly BindableProperty FillColorProperty =
            BindableProperty.Create(nameof(FillColor), typeof(Color), typeof(SOOSeparate), Color.White);

        public Color FillColor
        {
            get => (Color)GetValue(FillColorProperty);
            set => SetValue(FillColorProperty, value);
        }

        public static readonly BindableProperty SeparateHeightProperty =
            BindableProperty.Create(nameof(SeparateHeight), typeof(int), typeof(SOOSeparate), 10);

        public int SeparateHeight
        {
            get => (int)GetValue(SeparateHeightProperty);
            set => SetValue(SeparateHeightProperty, value);
        }

        private void line_PaintSurface(object sender, SkiaSharp.Views.Forms.SKPaintSurfaceEventArgs args)
        {
            var info = args.Info;
            var surface = args.Surface;
            var canvas = surface.Canvas;

            canvas.Clear();

            var textPaint = new SKPaint
            {
                Color = FillColor.ToSKColor(),
                TextSize = 20,
                FakeBoldText = true,
                TextAlign = SKTextAlign.Left
            };

            var thickLinePaint = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                Color = FillColor.ToSKColor(),
                StrokeWidth = 50
            };

            var xText = info.Width / 2;
            var xLine1 = 100;
            var xLine2 = info.Width - xLine1;
            var y = textPaint.FontSpacing;

            // Display text
            canvas.DrawText(Title, xText, y, textPaint);
            y += textPaint.FontSpacing;

            // Display thick line
            thickLinePaint.StrokeCap = SKStrokeCap.Round;
            canvas.DrawLine(xLine1, y, xLine2, y, thickLinePaint);
        }
    }
}
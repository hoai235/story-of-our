﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTMS.Xamarin.Core.Models;

namespace SOO.Commons.Shared.Services
{
    public interface IMediaService
    {
        event EventHandler<MediaFile> OnMediaPicked;
        event EventHandler<IList<MediaFile>> OnMediaPickedCompleted;
        Task<IList<MediaFile>> PickPhotosAsync();
        Task<IList<MediaFile>> PickVideosAsync();
        void Clean();
    }
}

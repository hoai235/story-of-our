﻿namespace SOO.Commons
{
    public static class Constants
    {
        public const string ApplicationName = "SOO";
        public const int PageSize = 10;

        public static class MessageCenters
        {
            public const string CLOSE_SWIPE_ITEM = "CLOSE_SWIPE_ITEM";
            public const string UPDATE_TOTAL_COST = "UPDATE_TOTAL_COST";
            public const string UPDATE_TOTAL_GALLEY = "UPDATE_TOTAL_GALLEY";
            public const string RESET_SELECT_STORY = "RESET_SELECT_STORY";
            public const string UPDATE_ORDER_ITEM = "UPDATE_ORDER_ITEM";
            public const string SCREEN_ORIENTATION = "SCREEN_ORIENTATION";
        }
    }
}

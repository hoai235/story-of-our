﻿using Autofac;
using SOO.Commons.Shared.Services;
using SOO.Droid.Core.Services;
using Xamarin.Forms;

namespace SOO.Droid.Core.Container
{
    public class AndroidContainer : SOOContainer
    {
        protected override void RegisterServices(ContainerBuilder containerBuilder)
        {
            DependencyService.RegisterSingleton<IMediaService>(MediaService.SharedInstance);
            base.RegisterServices(containerBuilder);
        }
    }
}
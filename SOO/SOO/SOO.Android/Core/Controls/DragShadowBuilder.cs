﻿using System;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;

namespace SOO.Droid.Core.Controls
{
    public class DragShadowBuilder : View.DragShadowBuilder
    {
        private readonly Drawable _shadow;

        [Obsolete]
        public DragShadowBuilder(View v)
            : base(v)
        {
            v.DrawingCacheEnabled = true;
            var bm = v.DrawingCache;
            _shadow = new BitmapDrawable(bm);
            _shadow.SetColorFilter(Color.ParseColor("#4EB1FB"), PorterDuff.Mode.Multiply);
        }

        public override void OnProvideShadowMetrics(Point size, Point touch)
        {
            var width = View.Width;
            var height = View.Height;
            _shadow.SetBounds(0, 0, width, height);
            size.Set(width, height);
            touch.Set(width / 2, height / 2);
        }

        public override void OnDrawShadow(Canvas canvas)
        {
            base.OnDrawShadow(canvas);
            _shadow.Draw(canvas);
        }
    }
}
﻿using Android.Content;
using Android.Views;
using SOO.Commons.Shared.Controls;
using SOO.Droid.Core.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ListView = Android.Widget.ListView;

[assembly:ExportRenderer(typeof(SOOViewCell), typeof(SOOViewCellRenderer))]
namespace SOO.Droid.Core.Controls
{
    public class SOOViewCellRenderer : ViewCellRenderer
    {
        private Android.Views.View _cellCore;

        protected override Android.Views.View GetCellCore(Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
        {
            _cellCore = base.GetCellCore(item, convertView, parent, context);

            if (!(parent is ListView listview))
            {
                return _cellCore;
            }

            listview.SetSelector(Android.Resource.Color.Transparent);
            listview.CacheColorHint = Android.Graphics.Color.Transparent;
            listview.VerticalScrollBarEnabled = false;
            return _cellCore;
        }
    }
}
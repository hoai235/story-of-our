﻿using Android.Content;
using SOO.Commons.Shared.Controls;
using SOO.Droid.Core.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(SOOListView), typeof(SOOListViewRenderer))]
namespace SOO.Droid.Core.Controls
{
    public class SOOListViewRenderer : ListViewRenderer
    {
        private readonly Context _context;
        public SOOListViewRenderer(Context context) : base(context)
        {
            _context = context;
        }

        [System.Obsolete]
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            
            Control.ItemLongClick += (s, args) =>
            {
                var data = ClipData.NewPlainText("List", args.Position.ToString());
                var shadowScreen = new DragShadowBuilder(args.View);
                args.View.StartDrag(data, shadowScreen, null, 0);
            };
        }
    }
}
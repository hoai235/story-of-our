﻿using System;
using System.Collections;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using SOO.Commons;
using SOO.Commons.Shared.Controls;
using SOO.Commons.Shared.Models;
using SOO.Droid.Core.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(SOOViewCellDrag), typeof(SOOViewCellDragRenderer))]

namespace SOO.Droid.Core.Controls
{
    public class SOOViewCellDragRenderer : ViewCellRenderer
    {
        public ListView ParentListView { get; set; }
        public Android.Widget.ListView ListView { get; set; }
        public ViewGroup ViewGroup { get; set; }
        public static int FirstIndex { get; set; } = -1;
        public static int SecondIndex { get; set; } = -1;
        public IList Items { get; set; }

        protected override View GetCellCore(Cell item, View view, ViewGroup parent, Context context)
        {
            if (parent is Android.Widget.ListView listView)
            {
                listView.SetSelector(Android.Resource.Color.Transparent);
                listView.CacheColorHint = Android.Graphics.Color.Transparent;
                listView.VerticalScrollBarEnabled = false;
            }

            ParentListView = item.ParentView as ListView;

            if (ParentListView != null)
            {
                Items = ParentListView.ItemsSource as IList;
            }

            var cellcore = base.GetCellCore(item, view, parent, context);

            cellcore.Drag -= CellCoreOnDrag;
            cellcore.Drag += CellCoreOnDrag;

            return cellcore;
        }

        private void CellCoreOnDrag(object sender, View.DragEventArgs args)
        {
            ViewGroup = sender as ViewGroup;
            var view = ViewGroup.Parent as View;
            ListView = ViewGroup.Parent.Parent as Android.Widget.ListView;

            switch (args.Event.Action)
            {
                case DragAction.Started:
                    args.Handled = true;
                    break;
                case DragAction.Entered:
                    args.Handled = true;
                    if (ListView != null && FirstIndex == -1)
                    {
                        FirstIndex = ListView.GetPositionForView(view);
                    }
                    break;
                case DragAction.Exited:
                    args.Handled = true;
                    break;
                case DragAction.Drop:
                    args.Handled = true;
                    if (SecondIndex == -1)
                    {
                        SecondIndex = ListView.GetPositionForView(view);
                    }

                    if (FirstIndex != -1)
                    {
                        FirstIndex = FirstIndex == 0 ? 0 : FirstIndex - 1;
                        SecondIndex = SecondIndex == 0 ? 0 : SecondIndex - 1;

                        dynamic firstItem = Items[FirstIndex];
                        dynamic secondItem = Items[SecondIndex];

                        firstItem.Order = SecondIndex + 1;
                        secondItem.Order = FirstIndex + 1;

                        Items.Remove(firstItem);
                        Items.Insert(SecondIndex, firstItem);
                        Items.Remove(secondItem);
                        Items.Insert(FirstIndex, secondItem);
                        ParentListView.ItemsSource = null;
                        ParentListView.ItemsSource = Items;
                    }
                    FirstIndex = -1;
                    SecondIndex = -1;
                    break;
                case DragAction.Ended:
                    args.Handled = true;
                    break;
                case DragAction.Location:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }
}
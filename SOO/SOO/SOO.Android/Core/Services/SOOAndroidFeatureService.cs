﻿using Android.App;
using Android.OS;
using Android.Widget;
using DTMS.Xamarin.Android.Core.Services;
using DTMS.Xamarin.Core.Interfaces;
using Java.IO;

[assembly: Xamarin.Forms.Dependency(typeof(DTMSAndroidFeatureService))]

namespace DTMS.Xamarin.Android.Core.Services
{
    public class DTMSAndroidFeatureService : IDTMSAndroidFeatureService
    {
        public void ShowMessage(string message, bool IsShowLong = false)
        {
            var mainHandler = new Handler(Looper.MainLooper);
            var runnableToast = new Java.Lang.Runnable(() =>
            {
                var duration = IsShowLong ? ToastLength.Long : ToastLength.Short;
                Toast.MakeText(Application.Context, message, duration).Show();
            });

            _ = mainHandler.Post(runnableToast);
        }

        public void CloseApp()
        {
            Process.KillProcess(Process.MyPid());
        }

        public string GetFolderLocation(string folderName)
        {
            var root = Environment.IsExternalStorageEmulated 
                ? Environment.ExternalStorageDirectory.AbsolutePath
                : System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);

            var folderPath = $"{root}/{folderName}";

            using (var myDir = new File(folderPath))
            {
                if (!myDir.Exists())
                    myDir.Mkdir();
            }

            return folderPath;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Database;
using Android.Graphics;
using Android.Media;
using Android.Provider;
using Android.Widget;
using DTMS.Xamarin.Core.Helpers;
using DTMS.Xamarin.Core.Models;
using Plugin.CurrentActivity;
using SOO.Commons.Shared.Services;
using SOO.Droid.Core.Helpers;
using SOO.Droid.Core.Services;
using Xamarin.Forms;


[assembly: Dependency(typeof(MediaService))]
namespace SOO.Droid.Core.Services
{
    public class MediaService : IMediaService
    {
        public static MediaService SharedInstance = new MediaService();
        private const int MultiPickerResultCode = 9793;
        private const string TemporalDirectoryName = "TmpMedia";
        public event EventHandler<MediaFile> OnMediaPicked;
        public event EventHandler<IList<MediaFile>> OnMediaPickedCompleted;

        private TaskCompletionSource<IList<MediaFile>> _mediaPickedTcs;

        public void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            ObservableCollection<MediaFile> mediaPicked = null;

            if (requestCode == MultiPickerResultCode)
            {
                if (resultCode == Result.Ok)
                {
                    mediaPicked = new ObservableCollection<MediaFile>();
                    if (data != null)
                    {
                        var clipData = data.ClipData;
                        if (clipData != null)
                        {
                            for (var i = 0; i < clipData.ItemCount; i++)
                            {
                                var item = clipData.GetItemAt(i);
                                var uri = item.Uri;
                                var media = CreateMediaFileFromUri(uri);
                                
                                if (media != null)
                                {
                                    mediaPicked.Add(media);
                                    OnMediaPicked?.Invoke(this, media);
                                }

                            }
                        }
                        else
                        {
                            var uri = data.Data;
                            var media = CreateMediaFileFromUri(uri);
                            if (media != null)
                            {
                                mediaPicked.Add(media);
                                OnMediaPicked?.Invoke(this, media);
                            }
                        }

                        OnMediaPickedCompleted?.Invoke(this, mediaPicked);
                    }
                }

                _mediaPickedTcs?.TrySetResult(mediaPicked);
            }
        }

        private static MediaFile CreateMediaFileFromUri(Android.Net.Uri uri)
        {
            MediaFile mediaFile = null;
            var type = CrossCurrentActivity.Current.Activity.ContentResolver.GetType(uri);

            var path = GetRealPathFromURI(uri);
            if (path != null)
            {
                var fullPath = string.Empty;
                var thumbnailImagePath = string.Empty;
                var fileName = System.IO.Path.GetFileNameWithoutExtension(path);
                var ext = System.IO.Path.GetExtension(path);
                var mediaFileType = MediaFileType.Image;

                if (type.StartsWith(Enum.GetName(typeof(MediaFileType), MediaFileType.Image) ?? string.Empty, StringComparison.CurrentCultureIgnoreCase))
                {
                    var fullImage = ImageHelpers.RotateImage(path, 1);
                    //var thumbImage = ImageHelpers.RotateImage(path, 1);
                    var bitmap = ThumbnailUtils.CreateImageThumbnail(path, ThumbnailKind.MiniKind);

                    fullPath = FileHelper.GetOutputPath(MediaFileType.Image, TemporalDirectoryName, $"{fileName}{ext}");
                    File.WriteAllBytes(fullPath, fullImage);

                    thumbnailImagePath = FileHelper.GetOutputPath(MediaFileType.Image, TemporalDirectoryName, $"{fileName}-THUMBNAIL{ext}");
                    var stream = new FileStream(thumbnailImagePath, FileMode.Create);
                    bitmap?.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                    stream.Close();
                    //File.WriteAllBytes(thumbnailImagePath,  bitmap.ToArray<byte>());

                }
                else if (type.StartsWith(Enum.GetName(typeof(MediaFileType), MediaFileType.Video) ?? string.Empty, StringComparison.CurrentCultureIgnoreCase))
                {
                    fullPath = path;
                    var bitmap = ThumbnailUtils.CreateVideoThumbnail(path, ThumbnailKind.MiniKind);

                    thumbnailImagePath = FileHelper.GetOutputPath(MediaFileType.Image, TemporalDirectoryName, $"{fileName}-THUMBNAIL{ext}");
                    var stream = new FileStream(thumbnailImagePath, FileMode.Create);
                    bitmap?.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                    stream.Close();

                    mediaFileType = MediaFileType.Video;
                }

                if (!string.IsNullOrEmpty(fullPath) && !string.IsNullOrEmpty(thumbnailImagePath))
                {
                    mediaFile = new MediaFile()
                    {
                        Path = fullPath,
                        Type = mediaFileType,
                        PreviewPath = thumbnailImagePath
                    };
                }

            }

            return mediaFile;
        }

        [Obsolete]
        public static string GetRealPathFromURI(Android.Net.Uri contentURI)
        {
            ICursor cursor = null;
            try
            {

                string mediaPath;
                cursor = CrossCurrentActivity.Current.Activity.ContentResolver.Query(contentURI, null, null, null, null);
                cursor.MoveToFirst();
                var idx = cursor.GetColumnIndex(MediaStore.MediaColumns.Data);

                if (idx != -1)
                {
                    var type = CrossCurrentActivity.Current.Activity.ContentResolver.GetType(contentURI);
                    var pIdx = cursor.GetColumnIndex(MediaStore.MediaColumns.Id);
                    var mData = cursor.GetString(idx);
                    mediaPath = mData;

                }
                else
                {

                    var docID = DocumentsContract.GetDocumentId(contentURI);
                    var doc = docID.Split(':');
                    var id = doc[1];
                    var whereSelect = MediaStore.Images.ImageColumns.Id + "=?";
                    var dataConst = MediaStore.Images.ImageColumns.Data;
                    var projections = new string[] { dataConst };
                    var internalUri = MediaStore.Images.Media.InternalContentUri;
                    var externalUri = MediaStore.Images.Media.ExternalContentUri;
                    switch (doc[0])
                    {
                        case "video":
                            internalUri = MediaStore.Video.Media.InternalContentUri;
                            externalUri = MediaStore.Video.Media.ExternalContentUri;
                            whereSelect = MediaStore.Video.VideoColumns.Id + "=?";
                            dataConst = MediaStore.Video.VideoColumns.Data;
                            break;
                        case "image":
                            whereSelect = MediaStore.Video.VideoColumns.Id + "=?";
                            projections = new string[] { MediaStore.Video.VideoColumns.Data };
                            break;
                    }

                    projections = new string[] { dataConst };
                    cursor = CrossCurrentActivity.Current.Activity.ContentResolver.Query(internalUri, projections, whereSelect, new string[] { id }, null);
                    if (cursor.Count == 0)
                    {
                        cursor = CrossCurrentActivity.Current.Activity.ContentResolver.Query(externalUri, projections, whereSelect, new string[] { id }, null);
                    }
                    var colDatax = cursor.GetColumnIndexOrThrow(dataConst);
                    cursor.MoveToFirst();

                    mediaPath = cursor.GetString(colDatax);
                }
                return mediaPath;
            }
            catch (Exception)
            {
                Toast.MakeText(CrossCurrentActivity.Current.Activity, "Unable to get path", ToastLength.Long).Show();

            }
            finally
            {
                if (cursor != null)
                {
                    cursor.Close();
                    cursor.Dispose();
                }
            }

            return null;

        }

        public void Clean()
        {

            var documentsDirectory = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), TemporalDirectoryName);

            if (Directory.Exists(documentsDirectory))
            {
                Directory.Delete(documentsDirectory);
            }
        }

        public async Task<IList<MediaFile>> PickPhotosAsync()
        {
            return await PickMediaAsync("image/*", "Select Images", MultiPickerResultCode);
        }

        public async Task<IList<MediaFile>> PickVideosAsync()
        {
            return await PickMediaAsync("video/*", "Select Videos", MultiPickerResultCode);
        }

        private async Task<IList<MediaFile>> PickMediaAsync(string type, string title, int resultCode)
        {
            _mediaPickedTcs = new TaskCompletionSource<IList<MediaFile>>();

            var imageIntent = new Intent(Intent.ActionPick);
            imageIntent.SetType(type);
            imageIntent.PutExtra(Intent.ExtraAllowMultiple, true);
            CrossCurrentActivity.Current.Activity.StartActivityForResult(Intent.CreateChooser(imageIntent, title), resultCode);

            return await _mediaPickedTcs.Task;
        }
    }
}
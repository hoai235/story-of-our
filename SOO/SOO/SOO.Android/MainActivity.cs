﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using CommonServiceLocator;
using DLToolkit.Forms.Controls;
using DTMS.Xamarin.Core.Interfaces;
using FFImageLoading.Forms.Platform;
using SOO.Droid.Core.Container;
using XF.Material.Droid;
using Plugin.Permissions;
using SOO.Commons;
using SOO.Droid.Core.Services;
using Xamarin.Forms;

namespace SOO.Droid
{
    [Activity(Label = "Story Of Our", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true
        , ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            Forms.SetFlags("SwipeView_Experimental", "Shapes_Experimental");

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, savedInstanceState);
            Material.Init(this, savedInstanceState);
            FlowListView.Init();
            CachedImageRenderer.Init(true);

            var appContainer = new AndroidContainer();
            appContainer.Setup();

            var navigationService = ServiceLocator.Current.GetInstance<INavigationService>();
            LoadApplication(new App(navigationService));

            MessageChangeScreenOrientation();
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override void OnBackPressed()
        {
            Material.HandleBackButton(base.OnBackPressed);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            MediaService.SharedInstance.OnActivityResult(requestCode, resultCode, data);
        }

        private void MessageChangeScreenOrientation()
        {
            MessagingCenter.Subscribe<string>(this, Constants.MessageCenters.SCREEN_ORIENTATION, (data) =>
            {
                bool.TryParse(data, out var isAuto);
                RequestedOrientation = isAuto ? ScreenOrientation.Sensor : ScreenOrientation.Portrait;
            });
        }
    }
}
﻿using DTMS.Xamarin.Core.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DTMS.Xamarin.Core.Interfaces
{
    public interface ISqliteService
    {
        SQLiteAsyncConnection SqlConnection { get; set; }

        /// <summary>
        /// Initialize connect sqlite.
        /// </summary>
        /// <param name="databaseFilePath"></param>
        void InitializeConnection(string databaseFileName);

        /// <summary>
        /// Create tables of models inherit BaseEntityLite.
        /// </summary>
        /// <param name="typeOfAssembly">Input type class assembly want to create table.</param>
        void CreateTables(Type typeOfAssembly);

        /// <summary>
        /// Set timeout connect sqlite.
        /// </summary>
        /// <param name="mininutes"></param>
        void SetTimeOut(int mininutes);

        /// <summary>
        /// Insert object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<T> InsertAsync<T>(T item) where T : BaseEntitySqlite, new();

        /// <summary>
        /// Update object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<T> UpdateAsync<T>(T item) where T : BaseEntitySqlite, new();

        /// <summary>
        /// Delete object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<T> DeleteAsync<T>(T item) where T : BaseEntitySqlite, new();

        /// <summary>
        /// Soft delete object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        Task<T> SoftDeleteAsync<T>(T item) where T : BaseEntitySqlite, new();

        /// <summary>
        /// Get object by id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string id) where T : BaseEntitySqlite, new();

        /// <summary>
        /// Get all item of table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<int> Count<T>() where T : BaseEntitySqlite, new();

        /// <summary>
        /// Get all object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync<T>(Func<T, bool> condition = null) where T : BaseEntitySqlite, new();

        /// <summary>
        /// Get paging object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page">Input page.</param>
        /// <param name="pageSize">Input page size want get</param>
        /// <param name="condition"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync<T>(int page, int pageSize, Func<T, bool> condition = null) where T : class, new();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> InsertRangeAsync<T>(IEnumerable<T> items);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> UpdateRangeAsync<T>(IEnumerable<T> items);
    }
}

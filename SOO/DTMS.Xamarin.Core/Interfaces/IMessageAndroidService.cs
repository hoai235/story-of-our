﻿namespace DTMS.Xamarin.Core.Interfaces
{
    public interface IMessageAndroidService
    {
        void ShowMessage(string message, bool IsShowLong = false);
    }
}
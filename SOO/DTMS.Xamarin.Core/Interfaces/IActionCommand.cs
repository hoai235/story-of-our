﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace DTMS.Xamarin.Core.Interfaces
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync();
        bool CanExecute();
    }

    public interface IAsyncCommand<in T> : ICommand
    {
        Task ExecuteAsync(T parameter);
        bool CanExecute(T parameter);
    }

    public interface IActionCommand : ICommand
    {
        void Execute();
        bool CanExecute();
    }

    public interface IActionCommand<in T> : ICommand
    {
        void Execute(T parameter); 
        bool CanExecute(T parameter);
    }
}

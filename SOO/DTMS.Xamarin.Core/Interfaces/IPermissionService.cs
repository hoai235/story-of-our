﻿using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;

namespace DTMS.Xamarin.Core.Interfaces
{
    public interface IPermissionService
    {
        Task<PermissionStatus> GetPermissionStatusAsync(Permission permission);

        //Task<PermissionStatus> GetPermissionStatusAsync(Permission[] permissions);
    }
}
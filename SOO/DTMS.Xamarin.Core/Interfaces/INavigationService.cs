﻿using System.Threading.Tasks;

namespace DTMS.Xamarin.Core.Interfaces
{
    public interface INavigationService
    {
        /// <summary>
        /// Check page current is root.
        /// </summary>
        /// <returns></returns>
        bool IsPageRoot();

        /// <summary>
        /// Get paramater of root page.
        /// </summary>
        /// <returns></returns>
        object GetParameter();

        /// <summary>
        /// Set root page of application.
        /// </summary>
        /// <param name="rootViewName"></param>
        /// <param name="parameter"></param>
        void SetRootView(string rootViewName, object parameter = null);

        Task PushAsync(string viewName, object parameter = null);

        Task PopAsync();

        Task PushModalAsync(string viewName, object parameter = null);

        Task PopModalAsync(object navigationParameter = null);
    }
}
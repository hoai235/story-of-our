﻿using System;
using System.Threading.Tasks;

namespace DTMS.Xamarin.Core.Interfaces
{
    public interface ISnackbarService
    {
        Task ShowSuccess(string message);
        Task ShowError(string errorMessage);
        Task<bool> ShowMessageButton(string message, string actionButtonText = "Got it", Action action = null);
        Task Loading(string message, Action action);
    }
}
﻿namespace DTMS.Xamarin.Core.Interfaces
{
    public interface IDTMSAndroidFeatureService
    {
        void CloseApp();

        string GetFolderLocation(string folderName);

        void ShowMessage(string message, bool IsShowLong = false);
    }
}
﻿using System.Threading.Tasks;

namespace DTMS.Xamarin.Core.Interfaces
{
    public interface IMediaPickerService
    {
        /// <summary>
        /// Pick image from library image of device.
        /// </summary>
        /// <returns>If has pick will return string base 64.</returns>
        Task<string> PickImageAsBase64String();

        /// <summary>
        /// Pick image from library image of device.
        /// </summary>
        /// /// <param name="extens"></param>
        /// <returns>If has pick will return path image.</returns>
        Task<string> PickImageAsPath(string[] extens = null);
    }
}
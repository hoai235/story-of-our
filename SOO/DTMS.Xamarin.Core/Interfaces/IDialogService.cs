﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs.Configurations;

namespace DTMS.Xamarin.Core.Interfaces
{
    public interface IDialogService
    {
        Task Loading(string message, Action action, MaterialLoadingDialogConfiguration configuration = null);

        Task<bool?> ShowView(View view, string title, string message = null, string confirmingText = "Save", string dismissiveText = "Close");

        Task<bool?> Confirm(string title = "Message",
                            string message = "Do you want delete this item ?",
                            string confirmingText = "Delete", string dismissiveText = "Cancel");

        Task<object> ShowSemiPage(View view, double heightView, string submitText = "OK");

        Task ShowSuccess(string message, string title = "Message");

        Task ShowError(string message, string title = "Message");

        Task<string> SingleInputAsync(string inputText, string inputPlaceHolder, string title, string message = null,
            string confirmingText = "Save", string dismissiveText = "Close");
    }
}
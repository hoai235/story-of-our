﻿using Autofac;
using Autofac.Extras.CommonServiceLocator;
using CommonServiceLocator;
using DTMS.Xamarin.Core.Interfaces;
using DTMS.Xamarin.Core.Services;
using System;

namespace DTMS.Xamarin.Core.IoC
{
    public abstract class AppContainer
    {
        IContainer container;

        public void Setup()
        {
            var containerBuilder = new ContainerBuilder();

            this.RegisterServices(containerBuilder);

            container = containerBuilder.Build();
            
            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));

            container.BeginLifetimeScope();
        }

        public T Resolve<T>()
        {
            if (container == null)
                throw new InvalidOperationException("Initialize object container");

            return container.Resolve<T>();
        }

        protected virtual void RegisterServices(ContainerBuilder containerBuilder)
        {
            // Service
            containerBuilder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();
            containerBuilder.RegisterType<PermissionService>().As<IPermissionService>().SingleInstance();
            containerBuilder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();
            containerBuilder.RegisterType<MediaPickerService>().As<IMediaPickerService>().SingleInstance();
            containerBuilder.RegisterType<SqliteService>().As<ISqliteService>().SingleInstance();
            containerBuilder.RegisterType<SnackbarService>().As<ISnackbarService>().SingleInstance();
        }
    }
}
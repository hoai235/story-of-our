﻿using System;
using Xamarin.Forms;

namespace DTMS.Xamarin.Core.Controls
{
    public class DTMSGrid : Grid
    {
        #region Private Member

        private double _gestureX { get; set; }
        private double _gestureY { get; set; }
        private bool IsSwipe { get; set; }

        #endregion Private Member

        /* ==================================================================== */

        #region Public Member

        #region Tapped

        public event EventHandler Tapped;

        protected void OnTapped(EventArgs e)
        {
            Tapped?.Invoke(this, e);
        }

        #endregion Tapped

        /* ==================================================================== */

        #region SwipeUP

        public event EventHandler SwipeUP;

        protected void OnSwipeUP(EventArgs e)
        {
            SwipeUP?.Invoke(this, e);
        }

        #endregion SwipeUP

        /* ==================================================================== */

        #region SwipeDown

        public event EventHandler SwipeDown;

        protected void OnSwipeDown(EventArgs e)
        {
            SwipeDown?.Invoke(this, e);
        }

        #endregion SwipeDown

        /* ==================================================================== */

        #region SwipeRight

        public event EventHandler SwipeRight;

        protected void OnSwipeRight(EventArgs e)
        {
            if (SwipeRight != null)
                SwipeRight(this, e);
        }

        #endregion SwipeRight

        /* ==================================================================== */

        #region SwipeLeft

        public event EventHandler SwipeLeft;

        protected void OnSwipeLeft(EventArgs e)
        {
            if (SwipeLeft != null)
                SwipeLeft(this, e);
        }

        #endregion SwipeLeft

        /* ==================================================================== */

        public double Height
        {
            get
            {
                return HeightRequest;
            }
            set
            {
                HeightRequest = value;
            }
        }

        public double Width
        {
            get
            {
                return WidthRequest;
            }
            set
            {
                WidthRequest = value;
            }
        }

        #endregion Public Member

        /* ==================================================================== */

        public DTMSGrid()
        {
            var panGesture = new PanGestureRecognizer();
            panGesture.PanUpdated += PanGesture_PanUpdated;

            var tapGesture = new TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            GestureRecognizers.Add(panGesture);
            GestureRecognizers.Add(tapGesture);
        }

        private void TapGesture_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (!IsSwipe)
                    OnTapped(null);

                IsSwipe = false;
            }
            catch
            {
                throw;
            }
        }

        private void PanGesture_PanUpdated(object sender, PanUpdatedEventArgs e)
        {
            try
            {
                switch (e.StatusType)
                {
                    case GestureStatus.Running:
                        {
                            _gestureX = e.TotalX;
                            _gestureY = e.TotalY;
                        }
                        break;

                    case GestureStatus.Completed:
                        {
                            IsSwipe = true;
                            if (Math.Abs(_gestureX) > Math.Abs(_gestureY))
                            {
                                if (_gestureX > 0)
                                {
                                    OnSwipeRight(null);
                                }
                                else
                                {
                                    OnSwipeLeft(null);
                                }
                            }
                            else
                            {
                                if (_gestureY > 0)
                                {
                                    OnSwipeDown(null);
                                }
                                else
                                {
                                    OnSwipeUP(null);
                                }
                            }
                        }
                        break;
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DTMS.Xamarin.Core.Controls
{
    public class DTMSToolbarItem : ToolbarItem
    {
        [Obsolete]
        public DTMSToolbarItem() : base()
        {
            this.InitVisibility();
        }

        [Obsolete]
        private async void InitVisibility()
        {
            await Task.Delay(100);
            OnIsVisibleChanged(this, false, IsVisible);
        }

        public new ContentPage Parent { set; get; }

        [Obsolete]
        public bool IsVisible
        {
            get => (bool)GetValue(IsVisibleProperty);
            set => SetValue(IsVisibleProperty, value);
        }

        [Obsolete]
        public static BindableProperty IsVisibleProperty =
            BindableProperty.Create<DTMSToolbarItem, bool>(o => o.IsVisible, false, propertyChanged: OnIsVisibleChanged);

        private static void OnIsVisibleChanged(BindableObject bindable, bool oldvalue, bool newvalue)
        {
            var item = bindable as DTMSToolbarItem;

            if (item != null && item.Parent == null)
                return;

            var items = item.Parent.ToolbarItems;

            if (newvalue && !items.Contains(item))
            {
                items.Add(item);
            }
            else if (!newvalue && items.Contains(item))
            {
                items.Remove(item);
            }
        }
    }
}
﻿using DTMS.Xamarin.Core.BaseXamarin;
using DTMS.Xamarin.Core.Utilities;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.UI;

namespace DTMS.Xamarin.Core.Controls
{
    internal class DTMSNavigationPage : MaterialNavigationPage
    {
        private const string SplashScreenName = "SplashScreen";
        private object _currentNavigationParameter;

        public DTMSNavigationPage(string rootViewName, object parameter = null) : base(ViewFactory.GetView(rootViewName))
        {
            _currentNavigationParameter = parameter;
        }

        public async Task PopViewAsync()
        {
            await this.Navigation.PopAsync(true);
            var count = this.Navigation.NavigationStack.Count - 1;
            var previousPage = (count < 0) ? this.Navigation.NavigationStack[0] : null;
            (previousPage?.BindingContext as BaseViewModel)?.OnViewBacked();
        }

        public async Task PushViewAsync(string rootViewName, object parameter = null)
        {
            _currentNavigationParameter = parameter;
            var view = ViewFactory.GetView(rootViewName);

            if (this.Navigation.NavigationStack.Count >= 1 && this.Navigation.NavigationStack.Contains(view))
                return;

            //view.Appearing += AppearingCurrentHandler;
            await this.Navigation.PushAsync(view);
        }

        public async Task PushModalAsync(string rootViewName, object parameter = null)
        {
            _currentNavigationParameter = parameter;
            var view = ViewFactory.GetView(rootViewName);

            if (this.Navigation.ModalStack.Count >= 1 && this.Navigation.ModalStack.Contains(view))
                return;

            view.Appearing += AppearingCurrentHandler;
            await this.Navigation.PushModalAsync(view);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public async Task PopModalAsync(object navigationParameter)
        {
            await this.Navigation.PopModalAsync(true);
            var count = this.Navigation.ModalStack.Count - 1;
            var previousPage = (count < 0) ? this.Navigation.NavigationStack[0] : this.Navigation.ModalStack[count];
            (previousPage.BindingContext as BaseViewModel)?.OnViewBacked(navigationParameter);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(this.RootPage) && this.RootPage != null)
            {
                this.RootPage.Appearing += this.AppearingRootHandler;
            }
        }

        protected override void OnPagePush(Page page)
        {
            base.OnPagePush(page);

            if (!(page.BindingContext is BaseViewModel viewModel)) return;
            viewModel?.OnViewPushed(_currentNavigationParameter);
            _currentNavigationParameter = null;
        }

        protected override void OnPagePop(Page previousPage, Page poppedPage)
        {
            if (previousPage.ToString() == SplashScreenName)
                return;

            base.OnPagePop(previousPage, poppedPage);

            if (previousPage.BindingContext is BaseViewModel viewModel)
            {
                viewModel.OnViewPopped();
            }
        }

        private void AppearingCurrentHandler(object sender, EventArgs e)
        {
            var page = (sender as Page);
            var viewModel = page.BindingContext as BaseViewModel;
            viewModel?.OnViewPushed(_currentNavigationParameter);
            _currentNavigationParameter = null;
            page.Appearing -= this.AppearingCurrentHandler;
        }

        private void AppearingRootHandler(object sender, EventArgs e)
        {
            var viewModel = this.RootPage.BindingContext as BaseViewModel;
            viewModel?.OnViewPushed(_currentNavigationParameter);
            _currentNavigationParameter = null;
            this.RootPage.Appearing -= this.AppearingRootHandler;
        }
    }
}
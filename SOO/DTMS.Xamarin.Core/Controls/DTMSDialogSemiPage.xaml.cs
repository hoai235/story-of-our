﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace DTMS.Xamarin.Core.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DTMSDialogSemiPage : BaseMaterialModalPage
    {
        public TaskCompletionSource<object> InputTaskCompletionSource { get; set; }

        private double HeightView { get; set; }

        public string SubmitText { get; set; }

        public DTMSDialogSemiPage()
        {
            this.Animation = new MoveAnimation(MoveAnimationOptions.Bottom, MoveAnimationOptions.Bottom)
            {
                DurationIn = 150,
                DurationOut = 300,
                EasingIn = Easing.SinOut,
                EasingOut = Easing.Linear,
                HasBackgroundAnimation = true,
            };

            InitializeComponent();
            this.InputTaskCompletionSource = new TaskCompletionSource<object>();
        }

        public static async Task<object> ShowAsync(View view, string submitText, double heightView)
        {
            var semi = new DTMSDialogSemiPage()
            {
                container = { Content = view ?? throw new ArgumentNullException(nameof(view)) },
                SubmitButton = { Text = submitText },
                HeightView = heightView
            };

            await semi.ShowAsync();

            return await semi.InputTaskCompletionSource.Task;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            SubmitButton.Clicked += SubmitButton_Clicked;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            SubmitButton.Clicked -= SubmitButton_Clicked;
        }

        private async void SubmitButton_Clicked(object sender, EventArgs e)
        {
            await this.DismissAsync();

            this.InputTaskCompletionSource.SetResult(null);
        }

        protected override void LayoutChildren(double x, double y, double width, double height)
        {
            //switch (DisplayOrientation)
            //{
            //    case Essentials.DisplayOrientation.Landscape when Device.Idiom == TargetIdiom.Phone:

            //        break;
            //    case Essentials.DisplayOrientation.Portrait when Device.Idiom == TargetIdiom.Phone:

            //        break;
            //}

            y = height - this.HeightView;

            base.LayoutChildren(x, y, width, this.HeightView);
        }
    }
}
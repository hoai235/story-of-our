﻿using Xamarin.Forms;

namespace DTMS.Xamarin.Core.Controls
{
    public class DTMSViewCell : ViewCell
    {
        public DTMSViewCell()
        {
        }

        public static BindableProperty BackgroundColorProperty = BindableProperty.Create(
            nameof(BackgroundColor),
            typeof(Color),
            typeof(DTMSViewCell),
            Color.White);

        public Color BackgroundColor
        {
            get => (Color)GetValue(BackgroundColorProperty);
            set => SetValue(BackgroundColorProperty, value);
        }

        public static BindableProperty SelectedColorProperty = BindableProperty.Create(
            nameof(SelectedColor),
            typeof(Color),
            typeof(DTMSViewCell),
            Color.White);

        public Color SelectedColor
        {
            get => (Color)GetValue(SelectedColorProperty);
            set => SetValue(SelectedColorProperty, value);
        }

        public static BindableProperty UnselectedColorProperty = BindableProperty.Create(
            nameof(UnselectedColor),
            typeof(Color),
            typeof(DTMSViewCell),
            Color.White);

        public Color UnselectedColor
        {
            get => (Color)GetValue(UnselectedColorProperty);
            set => SetValue(UnselectedColorProperty, value);
        }

        //protected override void OnAppearing()
        //{
        //    View.ScaleTo(1, 250, Easing.CubicIn);

        //    base.OnAppearing();
        //}
    }
}
﻿using DTMS.Xamarin.Core.Controls;
using DTMS.Xamarin.Core.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DTMS.Xamarin.Core.Services
{
    public class NavigationService : INavigationService
    {
        private DTMSNavigationPage _currentNavigationPage;

        private object _currentParameter;

        public async Task PopAsync()
        {
            if (_currentNavigationPage != null) await _currentNavigationPage.PopViewAsync();
        }

        public async Task PushAsync(string viewName, object parameter = null)
        {
            if (_currentNavigationPage != null) await _currentNavigationPage.PushViewAsync(viewName, parameter);
        }

        public async Task PushModalAsync(string viewName, object parameter = null)
        {
            if (_currentNavigationPage != null) await _currentNavigationPage.PushModalAsync(viewName, parameter);
        }

        public async Task PopModalAsync(object navigationParameter)
        {
            if (_currentNavigationPage != null) await _currentNavigationPage.PopModalAsync(navigationParameter);
        }

        public object GetParameter()
        {
            return _currentParameter;
        }

        public bool IsPageRoot()
        {
            return _currentNavigationPage.Navigation.ModalStack.Count == 0 && _currentNavigationPage.Navigation.NavigationStack.Count == 1;
        }

        public void SetRootView(string rootViewName, object parameter = null)
        {
            _currentNavigationPage = new DTMSNavigationPage(rootViewName, parameter);
            _currentParameter = parameter;

            Application.Current.MainPage = _currentNavigationPage;
        }
    }
}
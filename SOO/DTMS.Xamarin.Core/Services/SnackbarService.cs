﻿using DTMS.Xamarin.Core.Interfaces;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.Resources;
using XF.Material.Forms.UI.Dialogs;
using XF.Material.Forms.UI.Dialogs.Configurations;

namespace DTMS.Xamarin.Core.Services
{
    public class SnackbarService : ISnackbarService
    {
        public async Task ShowSuccess(string message)
        {
            await MaterialDialog.Instance.SnackbarAsync(message: message,
                                            msDuration: MaterialSnackbar.DurationShort);
        }

        public async Task<bool> ShowMessageButton(string message, string actionButtonText = "Got it", Action action = null)
        {
            var result = await MaterialDialog.Instance.SnackbarAsync(message: message,
                actionButtonText: actionButtonText,
                msDuration: MaterialSnackbar.DurationLong);

            if (result)
            {
                action?.Invoke();
            }

            return result;
        }

        public async Task ShowError(string errorMessage)
        {
            var configuration = new MaterialSnackbarConfiguration
            {
                BackgroundColor = XF.Material.Forms.Material.GetResource<Color>(MaterialConstants.Color.ERROR),
                MessageTextColor = XF.Material.Forms.Material.GetResource<Color>(MaterialConstants.Color.ON_SURFACE),
                TintColor = XF.Material.Forms.Material.GetResource<Color>(MaterialConstants.Color.SURFACE),
                CornerRadius = 4,
                ButtonAllCaps = false,
                ButtonFontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("FontFamily.RobotoBold"),
            };

            await MaterialDialog.Instance.SnackbarAsync(errorMessage, MaterialSnackbar.DurationShort, configuration);
        }

        public async Task Loading(string message, Action action)
        {
            using (await MaterialDialog.Instance.LoadingSnackbarAsync(message: message))
            {
                action();
            }
        }
    }
}
﻿using DTMS.Xamarin.Core.Interfaces;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;

namespace DTMS.Xamarin.Core.Services
{
    public class PermissionService : IPermissionService
    {
        public async Task<PermissionStatus> GetPermissionStatusAsync(Permission permission)
        {
            // check permission.
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
            if (status == PermissionStatus.Granted) return status;
            await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(permission);
            var results = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                
            return results.ContainsKey(permission) ? results[permission] : status;
        }
    }
}
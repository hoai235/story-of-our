﻿using DTMS.Xamarin.Core.Interfaces;
using DTMS.Xamarin.Core.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using NLog;

namespace DTMS.Xamarin.Core.Services
{
    public class SqliteService : ISqliteService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public SQLiteAsyncConnection SqlConnection { get; set; }

        #region Generates

        public void InitializeConnection(string databaseFileName)
        {
            SqlConnection = new SQLiteAsyncConnection(databaseFileName,
                SQLiteOpenFlags.Create | SQLiteOpenFlags.NoMutex | SQLiteOpenFlags.ReadWrite)
            {
                Tracer = query => { _logger.Debug($"Execute query {query} success "); },
                Trace = true
            };
        }

        public void CreateTables(Type typeOfAssembly)
        {
            var assembly = Assembly.GetAssembly(typeOfAssembly);
            var types = assembly.GetTypes().Where(type => type.BaseType != null && type.BaseType == typeof(BaseEntitySqlite)).ToArray();
            SqlConnection.CreateTablesAsync(CreateFlags.None, types);
        }

        /// <summary>
        /// Set timeout execute query sqlite.
        /// Default minutes = 2
        /// </summary>
        /// <param name="minutes">Input minutes.</param>
        public void SetTimeOut(int minutes = 2)
        {
            SqlConnection.SetBusyTimeoutAsync(new TimeSpan(0, minutes, 0));
        }

        #endregion Generates

        /* ================================================================================== */

        #region CRUD

        public async Task<IEnumerable<T>> GetAllAsync<T>(Func<T, bool> condition = null) where T : BaseEntitySqlite, new()
        {
            var items = await SqlConnection.Table<T>().ToListAsync();

            return condition != null ? items.Where(condition) : items;
        }

        public async Task<IEnumerable<T>> GetAllAsync<T>(int page, int pageSize, Func<T, bool> condition = null) where T : class, new()
        {
            if (page < 1)
                page = 1;

            if (pageSize < 1)
                pageSize = 10;

            var items = await SqlConnection.Table<T>().ToListAsync();

            if (condition != null)
            {
                items = items.Where(condition).ToList();
            }

            return items.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public async Task<int> Count<T>() where T : BaseEntitySqlite, new()
        {
            return await SqlConnection.Table<T>().Where(x => !x.IsDeleted).CountAsync();
        }

        public async Task<T> GetAsync<T>(string id) where T : BaseEntitySqlite, new()
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            return await SqlConnection.Table<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> InsertAsync<T>(T item) where T : BaseEntitySqlite, new()
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var success = await SqlConnection.InsertAsync(item, typeof(T));
            return success == 1 ? item : null;
        }

        public async Task<T> UpdateAsync<T>(T item) where T : BaseEntitySqlite, new()
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            item.UpdateDate = DateTime.Now;
            var success = await SqlConnection.UpdateAsync(item, typeof(T));
            return success == 1 ? item : null;
        }

        public async Task<T> SoftDeleteAsync<T>(T item) where T : BaseEntitySqlite, new()
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            item.IsDeleted = true;
            var success = await SqlConnection.UpdateAsync(item);
            return success == 1 ? item : null;
        }

        public async Task<T> DeleteAsync<T>(T item) where T : BaseEntitySqlite, new()
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var success = await SqlConnection.DeleteAsync<T>(item.Id);
            return success == 1 ? item : null;
        }

        public async Task<IEnumerable<T>> InsertRangeAsync<T>(IEnumerable<T> items)
        {
            var success = await SqlConnection.InsertAllAsync(items, typeof(T));
            return success == 1 ? items : null;
        }

        public async Task<IEnumerable<T>> UpdateRangeAsync<T>(IEnumerable<T> items)
        {
            var success = await SqlConnection.UpdateAllAsync(items);
            return success == 1 ? items : null;
        }

        #endregion CRUD

        /* ================================================================================== */
    }
}

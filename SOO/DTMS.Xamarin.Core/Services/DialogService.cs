﻿using System;
using System.Threading.Tasks;
using DTMS.Xamarin.Core.Controls;
using DTMS.Xamarin.Core.Interfaces;
using Xamarin.Forms;
using XF.Material.Forms;
using XF.Material.Forms.Resources;
using XF.Material.Forms.UI.Dialogs;
using XF.Material.Forms.UI.Dialogs.Configurations;

namespace DTMS.Xamarin.Core.Services
{
    public class DialogService : IDialogService
    {
        private IMaterialDialog DialogFacade => MaterialDialog.Instance;

        public async Task<bool?> Confirm(string title = "Message",
                                         string message = "Do you want delete this item ?",
                                         string confirmingText = "Delete", string dismissiveText = "Cancel")
        {
            return await DialogFacade.ConfirmAsync(title: title,
                                                   message: message,
                                                   confirmingText: confirmingText,
                                                   dismissiveText: dismissiveText);
        }

        public async Task Loading(string message, Action action, MaterialLoadingDialogConfiguration configuration = null)
        {
            using (await DialogFacade.LoadingDialogAsync(message, configuration))
            {
                action();
            }
        }

        public async Task ShowSuccess(string message, string title = "Message")
        {
            //var alertDialogConfiguration = new MaterialAlertDialogConfiguration
            //{
            //    BackgroundColor = XF.Material.Forms.Material.GetResource<Color>(MaterialConstants.Color.PRIMARY),
            //    TitleTextColor = XF.Material.Forms.Material.GetResource<Color>(MaterialConstants.Color.ON_PRIMARY),
            //    TitleFontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("FontFamily.Exo2Bold"),
            //    MessageTextColor = XF.Material.Forms.Material.GetResource<Color>(MaterialConstants.Color.ON_PRIMARY).MultiplyAlpha(0.8),
            //    MessageFontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("FontFamily.OpenSansRegular"),
            //    TintColor = XF.Material.Forms.Material.GetResource<Color>(MaterialConstants.Color.ON_PRIMARY),
            //    ButtonFontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("FontFamily.OpenSansSemiBold"),
            //    CornerRadius = 8,
            //    ScrimColor = Color.FromHex("#232F34").MultiplyAlpha(0.32),
            //    ButtonAllCaps = false
            //};

            await DialogFacade.AlertAsync(message, title);
        }

        public async Task ShowError(string message, string title = "Message")
        {
            await DialogFacade.AlertAsync(message, title);
        }

        public async Task<bool?> ShowView(View view, string title, string message = null, string confirmingText = "Save", string dismissiveText = "Close")
        {
            var configuration = new MaterialAlertDialogConfiguration
            {
                TitleFontFamily = Material.GetResource<OnPlatform<string>>("FontFamily.Bold"),
                MessageFontFamily = Material.GetResource<OnPlatform<string>>("FontFamily.Regular"),
                TintColor = Material.GetResource<Color>(MaterialConstants.Color.SECONDARY),
                MessageTextColor = Material.GetResource<Color>(MaterialConstants.Color.SECONDARY),
                ButtonFontFamily = Material.GetResource<OnPlatform<string>>("FontFamily.Bold"),
                CornerRadius = 4,
                ScrimColor = Material.GetResource<Color>(MaterialConstants.Color.PRIMARY).MultiplyAlpha(0.8),
                ButtonAllCaps = true
            };
            
            return await DialogFacade.ShowCustomContentAsync(view: view,
                                                             message: message,
                                                             title: title,
                                                             confirmingText: confirmingText,
                                                             dismissiveText: dismissiveText,
                                                             configuration: configuration);
        }

        public async Task<object> ShowSemiPage(View view, double heightView, string submitText = "OK")
        {
            return await DTMSDialogSemiPage.ShowAsync(view, submitText, heightView);
        }

        public async Task<string> SingleInputAsync(string inputText, string inputPlaceHolder, string title, string message = null, string confirmingText = "Save", string dismissiveText = "Close")
        {
            return await DialogFacade.InputAsync(title, message, inputText, inputPlaceHolder, confirmingText,
                dismissiveText);
        }
    }
}
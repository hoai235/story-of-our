﻿using DTMS.Xamarin.Core.Extensions;
using DTMS.Xamarin.Core.Interfaces;
using DTMS.Xamarin.Core.Utilities;
using Plugin.Media;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DTMS.Xamarin.Core.Services
{
    public class MediaPickerService : IMediaPickerService
    {
        /// <summary>
        /// Pick image from library image of device.
        /// </summary>
        /// <returns>If has pick will return string base 64.</returns>
        public async Task<string> PickImageAsBase64String()
        {
            string base64Str;

            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported) return null;
            var media = await CrossMedia.Current.PickPhotoAsync();

            using (var mediaStream = media.GetStream())
            using (var memStream = new MemoryStream())
            {
                await mediaStream.CopyToAsync(memStream);
                base64Str = Convert.ToBase64String(memStream.ToArray());
            }

            return base64Str;
        }

        /// <summary>
        /// Pick image from library image of device.
        /// </summary>
        /// <returns>If has pick will return path image.</returns>
        public async Task<string> PickImageAsPath(string[] extens = null)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
                return null;

            var media = await CrossMedia.Current.PickPhotoAsync();

            var imagePath = media.Path;

            if (!imagePath.ContainsAll(extens))
                throw new InvalidFormatFileException("Format file is invalid.");

            return imagePath;
        }
    }
}
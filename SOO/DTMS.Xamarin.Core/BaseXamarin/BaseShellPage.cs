﻿using Xamarin.Forms;

namespace DTMS.Xamarin.Core.BaseXamarin
{
    public abstract class BaseShellPage<TViewModel> : Shell where TViewModel : BaseViewModel
    {
        public TViewModel ViewModel { get; }

        protected BaseShellPage()
        {
            this.ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            this.BindingContext = this.ViewModel;
        }
    }
}
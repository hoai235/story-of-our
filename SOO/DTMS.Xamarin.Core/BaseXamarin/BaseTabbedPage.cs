﻿using Xamarin.Forms;

namespace DTMS.Xamarin.Core.BaseXamarin
{
    public abstract class BaseTabbedPage<TViewModel> : TabbedPage where TViewModel : BaseViewModel
    {
        public TViewModel ViewModel { get; }

        protected BaseTabbedPage()
        {
            this.ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            this.BindingContext = this.ViewModel;
        }
    }
}
﻿using CommonServiceLocator;
using DTMS.Xamarin.Core.Interfaces;
using DTMS.Xamarin.Core.Models;
using NLog;
using System.Collections.Generic;
using Xamarin.Forms;

namespace DTMS.Xamarin.Core.BaseXamarin
{
    public class BaseViewModel : ObservableObject
    {
        #region Avariables

        protected IServiceLocator ServiceLocator { get; }
        protected INavigationService NavigationService { get; }
        protected IDialogService DialogService { get; }
        protected ISnackbarService SnackbarService { get; }
        protected IDTMSAndroidFeatureService AndroidService { get; }
        protected readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion Avariables

        /* =================================================================================================== */

        #region Properities

        private string _title;
        private bool _isBusy;
        private List<string> _actions;

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        public List<string> Actions
        {
            get => _actions;
            set => SetProperty(ref _actions, value);
        }

        #endregion Properities

        /* =================================================================================================== */

        #region Constructors

        protected BaseViewModel()
        {
            this.ServiceLocator = CommonServiceLocator.ServiceLocator.Current;
            this.NavigationService = this.ServiceLocator.GetInstance<INavigationService>();
            this.DialogService = this.ServiceLocator.GetInstance<IDialogService>();
            this.SnackbarService = this.ServiceLocator.GetInstance<ISnackbarService>();
            this.AndroidService = DependencyService.Get<IDTMSAndroidFeatureService>();
        }

        #endregion Constructors

        /* =================================================================================================== */

        #region Methods

        /// <summary>
        /// When overriden, allow to add additional logic to this view model when the view where it was attached was pushed using <see cref="INavigationService.PushAsync(string, object)"/>.
        /// </summary>
        /// <param name="navigationParameter">The navigation parameter to pass after the view was pushed.</param>
        public virtual void OnViewPushed(object navigationParameter = null) { }

        /// <summary>
        ///
        /// </summary>
        /// <param name="navigationParameter"></param>
        public virtual void OnViewBacked(object navigationParameter = null) { }

        /// <summary>
        /// When overriden, allow to add additional logic to this view model when the view where it was attached was popped using <see cref="INavigationService.PopAsync"/>.
        /// </summary>
        public virtual void OnViewPopped()
        {
            this.ClearUp();
        }

        public virtual void ClearUp()
        {
        }

        public virtual void CloseApp()
        {
            Logger.Info("Exit application");

            LogManager.Shutdown();

            this.AndroidService.CloseApp();
        }

        #endregion Methods
    }
}
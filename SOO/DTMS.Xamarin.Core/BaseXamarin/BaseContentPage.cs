﻿using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace DTMS.Xamarin.Core.BaseXamarin
{
    public abstract class BaseContentPage<TViewModel> : ContentPage where TViewModel : BaseViewModel
    {
        public TViewModel ViewModel { get; }

        protected BaseContentPage()
        {
            this.ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            this.BindingContext = this.ViewModel;
            this.On<iOS>().SetUseSafeArea(true);
        }
    }
}
﻿using Xamarin.Forms;

namespace DTMS.Xamarin.Core.BaseXamarin
{
    public abstract class BaseMasterDetailPage<TViewModel> : MasterDetailPage where TViewModel : BaseViewModel
    {
        public TViewModel ViewModel { get; }

        protected BaseMasterDetailPage()
        {
            this.ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            this.BindingContext = this.ViewModel;
        }
    }
}
﻿using System;

namespace DTMS.Xamarin.Core.Models
{
    public class BaseModel
    {
        public string Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public int RowVersion { get; set; } = 1;

        public BaseModel()
        {
            Id = Guid.NewGuid().ToString();
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            IsDeleted = false;
        }
    }
}
﻿namespace DTMS.Xamarin.Core.Models
{
    public class InitModel
    {
        public string BackgroundSplashScreen { get; set; }
        public string ApplicationName { get; set; }
        public string MainPageName { get; set; }
    }
}
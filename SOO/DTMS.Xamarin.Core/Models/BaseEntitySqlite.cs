﻿using SQLite;
using System;

namespace DTMS.Xamarin.Core.Models
{
    public class BaseEntitySqlite
    {
        [PrimaryKey]
        public string Id
        {
            get;
            set;
        }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public int RowVersion { get; set; } = 1;

        protected BaseEntitySqlite()
        {
            Id = Guid.NewGuid().ToString().Replace("-", "");
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            IsDeleted = false;
        }
    }
}
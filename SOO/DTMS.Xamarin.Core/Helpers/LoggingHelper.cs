﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Text;

namespace DTMS.Xamarin.Core.Helpers
{
    public static class LoggingHelper
    {
        public static LoggingConfiguration GetConfiguration(string pathBase)
        {
            var config = new LoggingConfiguration();
            try
            {
                var fileTarget = new FileTarget("logFile")
                {
                    FileName = $"{pathBase}/{DateTime.Now:dd-MM-yyyy}.log",
                    Layout =
                    @"${date:format=yyyy\-MM\-dd HH\:mm\:ss}-${uppercase:${level}}:${exception} ${message}",
                    Encoding = Encoding.UTF8,
                    MaxArchiveFiles = 7,
                    ArchiveEvery = FileArchivePeriod.Day
                };
                config.AddTarget(fileTarget);

                config.AddRuleForOneLevel(LogLevel.Error, fileTarget);
                config.AddRuleForOneLevel(LogLevel.Debug, fileTarget);
                config.AddRuleForOneLevel(LogLevel.Info, fileTarget);
            }
            catch
            {
                return config;
            }

            return config;
        }
    }
}
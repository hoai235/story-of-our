﻿using System;
using System.IO;
using DTMS.Xamarin.Core.Models;

namespace DTMS.Xamarin.Core.Helpers
{
    public static class FileHelper
    {
        public static string GetUniquePath(MediaFileType type, string path, string name)
        {
            var ext = Path.GetExtension(name);
            if (ext == string.Empty)
                ext = ((type == MediaFileType.Image) ? ".jpg" : ".mp4");

            name = Path.GetFileNameWithoutExtension(name);

            var nameExt = name + ext;
            var i = 1;
            while (File.Exists(Path.Combine(path, nameExt)))
                nameExt = name + "_" + (i++) + ext;

            return Path.Combine(path, nameExt);
        }


        public static string GetOutputPath(MediaFileType type, string path, string name)
        {
            path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), path);
            Directory.CreateDirectory(path);
            if (!string.IsNullOrWhiteSpace(name)) 
                return Path.Combine(path, GetUniquePath(type, path, name));

            var timestamp = DateTime.Now.ToString("yyyMMdd_HHmmss");
            if (type == MediaFileType.Image)
                name = "IMG_" + timestamp + ".jpg";
            else
                name = "VID_" + timestamp + ".mp4";

            return Path.Combine(path, GetUniquePath(type, path, name));
        }
    }
}

﻿using System.Linq;
using Xamarin.Forms;

namespace DTMS.Xamarin.Core.Helpers
{
    public static class ResourcesHelper
    {
        public static object GetResource(string key)
        {
            return Application.Current.Resources.FirstOrDefault(x => x.Key.ToUpper() == key.ToUpper());
        }
    }
}

﻿namespace DTMS.Xamarin.Core.Helpers
{
    public class ApplicationHelper
    {
        public static string AppName { get; set; }

        public static bool UserSqlite { get; set; }

        public static void SetApplication(string appName, bool useSqlite)
        {
            AppName = appName;
            UserSqlite = useSqlite;
        }

        public static string GetFolderLog()
        {
            return $"{AppName}/Log";
        }

        public static string GetFolderDatabase()
        {
            return $"{AppName}/Database";
        }

        public static string GetSqliteName()
        {
            return $"{AppName}.db".ToLower();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DTMS.Xamarin.Core.Utilities
{
    public static class Extensions
    {
        #region Collections

        /// <summary>
        /// Extension paging of IEnumable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="page">Input page.</param>
        /// <param name="pageSize">Size a page.</param>
        /// <returns></returns>
        public static IEnumerable<T> Paging<T>(this IEnumerable<T> enumerable, int page, int pageSize)
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));

            return enumerable.Skip((page - 1) * pageSize).Take(pageSize);
        }

        /// <summary>
        /// Extension paging of IQuerable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryable"></param>
        /// <param name="page">Input page.</param>
        /// <param name="pageSize">Size a page.</param>
        /// <returns></returns>
        public static IQueryable<T> Paging<T>(this IQueryable<T> queryable, int page, int pageSize)
        {
            if (queryable == null)
                throw new ArgumentNullException(nameof(queryable));

            return queryable.Skip((page - 1) * pageSize).Take(pageSize);
        }

        /// <summary>
        /// Add object to IEnumerable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="value">Input object gereric</param>
        /// <returns></returns>
        public static IEnumerable<T> Add<T>(this IEnumerable<T> enumerable, T value)
        {
            foreach (var item in enumerable)
                yield return item;

            yield return value;
        }

        /// <summary>
        /// Remove object from IEnumerable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="index">Input index of element.</param>
        /// <returns></returns>
        public static IEnumerable<T> Remove<T>(this IEnumerable<T> enumerable, int index)
        {
            int current = 0;
            foreach (var item in enumerable)
            {
                if (current != index)
                    yield return item;

                current++;
            }
        }

        #endregion Collections

        /* ===================================================================== */

        #region DateTime

        /// <summary>
        /// Update start time of date. Ex: 2019-01-01 00:00:00
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime StartDate(this DateTime dateTime)
        {
            dateTime = new DateTime(
                       dateTime.Year,
                       dateTime.Month,
                       dateTime.Day,
                       0, 0, 0, 0,
                       dateTime.Kind);

            return dateTime;
        }

        /// <summary>
        /// Update last time of date. Ex: 2019-01-01 23:59:59
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime EndDate(this DateTime dateTime)
        {
            dateTime = new DateTime(
                       dateTime.Year,
                       dateTime.Month,
                       dateTime.Day,
                       23, 59, 59, 999,
                       dateTime.Kind);

            return dateTime;
        }

        #endregion DateTime

        /* ===================================================================== */

        #region String

        /// <summary>
        /// Check string contains in list string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="listItem"></param>
        /// <returns>If contains return true, false is contrary.</returns>
        public static bool ContainsAll(this string str, IEnumerable<string> listItem)
        {
            return listItem.All(item => str.ToUpper().Contains(item.ToUpper()));
        }

        #endregion String
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DTMS.Xamarin.Core.Extensions
{
    public static class ObservableCollectionExtensions
    {
        public static void AddRange<T>(this ObservableCollection<T> items, IEnumerable<T> inputs)
        {
            foreach (var input in inputs)
                items.Add(input);
        }

        public static void AddFirst<T>(this ObservableCollection<T> items, T item)
        {
            items.Insert(0, item);
        }

        public static void AddLast<T>(this ObservableCollection<T> items, T item)
        {
            items.Insert(items.Count - 1, item);
        }
    }
}

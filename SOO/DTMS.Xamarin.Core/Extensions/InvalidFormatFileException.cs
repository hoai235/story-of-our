﻿using System;

namespace DTMS.Xamarin.Core.Extensions
{
    public class InvalidFormatFileException : Exception
    {
        public InvalidFormatFileException()
        {
        }

        public InvalidFormatFileException(string message) : base(message)
        {
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DTMS.Xamarin.Core.Interfaces;
using Newtonsoft.Json;
using NLog;

namespace DTMS.Xamarin.Core.Behaviors
{

    public class AsyncCommand : IAsyncCommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private bool _isExecuting;
        private readonly Func<Task> _execute;
        private readonly Func<bool> _canExecute;

        public AsyncCommand(
            Func<Task> execute,
            Func<bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute()
        {
            return !_isExecuting && (_canExecute?.Invoke() ?? true);
        }

        public async Task ExecuteAsync()
        {
            if (!CanExecute())
            {
                throw new InvalidOperationException("Cannot execute command");
            }

            var methodName = _execute.Method.Name;
            _isExecuting = true;

            _logger.Debug($"Execute method {methodName} processing");

            var task = _execute.Invoke();

            await task.ContinueWith(async (taskContinue) =>
            {
                _logger.Debug($"Execute method {methodName} processed");

                if (taskContinue.IsFaulted)
                {
                    await CommonServiceLocator.ServiceLocator.Current.GetInstance<ISnackbarService>()
                        .ShowError("Something went wrong. Please retry !!!");
                    _logger.Error($"Execute method {methodName} failed exception {taskContinue.Exception}");
                }

                _logger.Info($"Execute command successfully");
                _isExecuting = false;
            });

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync();
        }
    }

    public class AsyncCommand<T> : IAsyncCommand<T>
    {
        public event EventHandler CanExecuteChanged;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private bool _isExecuting;
        private readonly Func<T, Task> _execute;
        private readonly Func<T, bool> _canExecute;

        public AsyncCommand(
            Func<T, Task> execute,
            Func<T, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(T parameter)
        {
            return !_isExecuting && (_canExecute?.Invoke(parameter) ?? true);
        }

        public async Task ExecuteAsync(T parameter)
        {
            if (!CanExecute(parameter))
            {
                throw new InvalidOperationException("Cannot execute command");
            }

            var methodName = _execute.Method.Name;
            try
            {
                _isExecuting = true;

                _logger.Debug($"Execute method {methodName} processing");
                await _execute.Invoke(parameter);
                _logger.Debug($"Execute method {methodName} processed");
            }
            catch (Exception ex)
            {
                await CommonServiceLocator.ServiceLocator.Current.GetInstance<ISnackbarService>()
                    .ShowError("Something went wrong. Please retry !!!");
                _logger.Error($"Execute method {methodName} failed exception {ex} with parameter {JsonConvert.SerializeObject(parameter)}");
            }
            finally
            {
                _logger.Info($"Execute command successfully");
                _isExecuting = false;
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync((T)parameter);
        }
    }
}

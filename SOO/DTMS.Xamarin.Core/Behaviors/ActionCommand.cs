﻿using System;
using System.Windows.Input;
using DTMS.Xamarin.Core.Interfaces;
using Newtonsoft.Json;
using NLog;

namespace DTMS.Xamarin.Core.Behaviors
{
    public class ActionCommand : IActionCommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private bool _isExecuting;
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public ActionCommand(
            Action execute,
            Func<bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute()
        {
            return !_isExecuting && (_canExecute?.Invoke() ?? true);
        }

        public void Execute()
        {
            if (!CanExecute())
            {
                throw new InvalidOperationException("Cannot execute command");
            }

            var methodName = _execute.Method.Name;
            try
            {
                _isExecuting = true;

                _logger.Debug($"Execute method {methodName} processing");

                _execute.Invoke();

                _logger.Debug($"Execute method {methodName} processed");
            }
            catch (Exception ex)
            {
                _logger.Error($"Execute method {methodName} failed exception {ex}");
            }
            finally
            {
                _isExecuting = false;
            }

            RaiseCanExecuteChanged();
        }
        //with parameter { JsonConvert.SerializeObject(parameter)

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            Execute();
        }
    }

    public class ActionCommand<T> : IActionCommand<T>
    {
        public event EventHandler CanExecuteChanged;

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private bool _isExecuting;
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public ActionCommand(
            Action<T> execute,
            Func<T, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(T parameter)
        {
            return !_isExecuting && (_canExecute?.Invoke(parameter) ?? true);
        }

        public void Execute(T parameter)
        {
            if (!CanExecute(parameter))
            {
                throw new InvalidOperationException("Cannot execute command");
            }

            var methodName = _execute.Method.Name;
            try
            {
                _isExecuting = true;

                _logger.Debug($"Execute method {methodName} processing");

                _execute.Invoke(parameter);

                _logger.Debug($"Execute method {methodName} processed");
            }
            catch (Exception ex)
            {
                _logger.Error($"Execute method {methodName} failed exception {ex} with parameter {JsonConvert.SerializeObject(parameter)}");
            }
            finally
            {
                _isExecuting = false;
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        void ICommand.Execute(object parameter)
        {
            Execute((T)parameter);
        }
    }
}

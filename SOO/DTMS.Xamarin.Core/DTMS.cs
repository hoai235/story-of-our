﻿using DTMS.Xamarin.Core.Helpers;

namespace DTMS.Xamarin.Core
{
    public class DTMS
    {
        /// <summary>
        /// Initialize Application.
        /// </summary>
        /// <param name="applicationName"></param>
        /// <param name="hasSqlite"></param>
        public static void Init(string applicationName, bool hasSqlite)
        {
            if (string.IsNullOrEmpty(applicationName))
                applicationName = "DTMS";

            ApplicationHelper.SetApplication(applicationName, hasSqlite);
        }
    }
}
﻿namespace DTMS.Xamarin.Core.Constants
{
    public class DTMSConstants
    {
        #region Utilities

        public static readonly int PAGE_SIZE = 10;

        public static readonly string SHOW_MAINPAGE = "ShowMainPage";

        #endregion Utilities
    }
}